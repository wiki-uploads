#A pre run setup check for LyxWin support programs
#Rob S

AcroRd32 

Gsview32

Gswin32

Yap

echo This is your version of LaTeX >setupcheck.txt
echo .............................................. >>setupcheck.txt
Latex --version >>setupcheck.txt
echo _____________________________________________ >>setupcheck.txt


echo This is your version of Image Magick >>setupcheck.txt
echo  .............................................>>setupcheck.txt
convert -version >>setupcheck.txt

notepad setupcheck.txt
del setupcheck.txt
Exit