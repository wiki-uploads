REM ***********************************************************************************************
REM *** A simple start-up script for LyX (www.lyx.org), written by Frode Egeland, 30/04/04      ***
REM *** Customise this file with your own paths to Ghostscript, LyX, MiKTeX and Acrobat Reader  ***
REM *** NOTE: You will need to have installed Ghostscript for Windows, LyX for Windows, MiKTeX, ***
REM *** and a PDF viewer, like Adobe Acrobat Reader. Also note that you should start each line  ***
REM *** of the PATH commands with PATH=%PATH%;  Then add your path behind the semi-colon (";")  ***
REM *** NOTE: Paths with spaced MUST be enclosed in double-quotes, eg: "C:\some path\here"      ***
REM ***********************************************************************************************
TITLE LyX Start-up Script


REM This should be the path to the Ghostscript \bin and \lib folders
PATH=%PATH%;C:\gs\gs8.14\bin;C:\gs\gs8.14\lib


REM Put the path to the LyX \bin and \share\lyx folders here
PATH=%PATH%;C:\LyX\lyx\bin;C:\LyX\lyx\share\lyx


REM This needs to be the path to MiKTeX's \bin folder
PATH=%PATH%;C:\texmf\miktex\bin


REM Path to Acrobat Reader goes here
PATH=%PATH%;"C:\Program Files\Adobe\Acrobat 6.0\Reader"


REM This starts LyX in it's own window. Note that this has to be a shortcut created from the \bin\lyx.exe to the main lyx folder
start C:\lyx\lyx.exe.lnk
