#!/bin/sh
# lyx-remote - opens files in a running LyX
# � 2002 Ronald Florence
# ron@18james.com, 16 Dec 2002, 18 May 2003

[ $# -ne 1 ] && { xmessage -nearmouse "Usage: $0 lyx-file"; exit; }
PIPE=`perl -n -e 'print if s/^.serverpipe\s+"(.+)"/$1/;' ~/.lyx/preferences`
[ "X$PIPE" = "X" ] && { xmessage -nearmouse "No lyxpipe defined."; exit; }

running=`ps -uxc | perl -n -e 'print if s/.+\s+(lyx)$/$1/;'`
if [ "X$running" = "X" ] 
  then 
  rm -f $PIPE.in $PIPE.out
  cd $HOME
  PATH=$PATH:/usr/local/bin:/sw/bin
  DISPLAY=${DISPLAY-localhost:0.0}
  export PATH DISPLAY
  lyx $@ &
else
  if [ -e ${PIPE}.in ]
  then
    echo "LYXCMD:lyx-remote:file-open:$1" > $PIPE.in
    read a < $PIPE.out
    echo $a
  else
    xmessage -nearmouse "Hmmm.  LyX is running but there is no server pipe."
  fi
fi
