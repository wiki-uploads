FasdUAS 1.101.10   ��   ��    k             l      ��  ��   lf
This script sends the currently selected entries 
of the active BibDesk document to LyX as citations.
It does this by `echo`ing a LYXCMD to the user's
lyxpipe (the location of the pipe is determined automatically).

This is my first bit of apple script so please let me
know if there are ways of improving this script.

For more information see:
	http://www.threewordslong.com/projects/misc/bibdesktolyx/

Contact: <mark *at* threewordslong *dot* com>

Mark Reid, 7th June 2005 
	
CHANGELOG:
25th November 2008 - Add support for LyX-1.6 and LyX-2.0 (Bennett Helm)
5th June 2006 - Add future support for LyX-1.5 (Bennett Helm)
3rd June 2006 - Update to automatically determine location of LyX pipe (Mark Reid)
27th Feb 2006 - Update to new location of LyX user's directory for 1.4.0 (Bennett Helm)
8th May 2005  - Updated to new location of LyX user's directory (Bennett Helm)
7th June 2005 - Initial Release (Mark Reid)

Copyright (C) 2005-2006 Mark Reid 

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	
     � 	 	� 
 T h i s   s c r i p t   s e n d s   t h e   c u r r e n t l y   s e l e c t e d   e n t r i e s   
 o f   t h e   a c t i v e   B i b D e s k   d o c u m e n t   t o   L y X   a s   c i t a t i o n s . 
 I t   d o e s   t h i s   b y   ` e c h o ` i n g   a   L Y X C M D   t o   t h e   u s e r ' s 
 l y x p i p e   ( t h e   l o c a t i o n   o f   t h e   p i p e   i s   d e t e r m i n e d   a u t o m a t i c a l l y ) . 
 
 T h i s   i s   m y   f i r s t   b i t   o f   a p p l e   s c r i p t   s o   p l e a s e   l e t   m e 
 k n o w   i f   t h e r e   a r e   w a y s   o f   i m p r o v i n g   t h i s   s c r i p t . 
 
 F o r   m o r e   i n f o r m a t i o n   s e e : 
 	 h t t p : / / w w w . t h r e e w o r d s l o n g . c o m / p r o j e c t s / m i s c / b i b d e s k t o l y x / 
 
 C o n t a c t :   < m a r k   * a t *   t h r e e w o r d s l o n g   * d o t *   c o m > 
 
 M a r k   R e i d ,   7 t h   J u n e   2 0 0 5   
 	 
 C H A N G E L O G : 
 2 5 t h   N o v e m b e r   2 0 0 8   -   A d d   s u p p o r t   f o r   L y X - 1 . 6   a n d   L y X - 2 . 0   ( B e n n e t t   H e l m ) 
 5 t h   J u n e   2 0 0 6   -   A d d   f u t u r e   s u p p o r t   f o r   L y X - 1 . 5   ( B e n n e t t   H e l m ) 
 3 r d   J u n e   2 0 0 6   -   U p d a t e   t o   a u t o m a t i c a l l y   d e t e r m i n e   l o c a t i o n   o f   L y X   p i p e   ( M a r k   R e i d ) 
 2 7 t h   F e b   2 0 0 6   -   U p d a t e   t o   n e w   l o c a t i o n   o f   L y X   u s e r ' s   d i r e c t o r y   f o r   1 . 4 . 0   ( B e n n e t t   H e l m ) 
 8 t h   M a y   2 0 0 5     -   U p d a t e d   t o   n e w   l o c a t i o n   o f   L y X   u s e r ' s   d i r e c t o r y   ( B e n n e t t   H e l m ) 
 7 t h   J u n e   2 0 0 5   -   I n i t i a l   R e l e a s e   ( M a r k   R e i d ) 
 
 C o p y r i g h t   ( C )   2 0 0 5 - 2 0 0 6   M a r k   R e i d   
 
 T h i s   p r o g r a m   i s   f r e e   s o f t w a r e ;   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r 
 m o d i f y   i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 a s   p u b l i s h e d   b y   t h e   F r e e   S o f t w a r e   F o u n d a t i o n ;   e i t h e r   v e r s i o n   2 
 o f   t h e   L i c e n s e ,   o r   ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n . 
 
 T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l , 
 b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f 
 M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e 
 G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s . 
 
 Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 a l o n g   w i t h   t h i s   p r o g r a m ;   i f   n o t ,   w r i t e   t o   t h e   F r e e   S o f t w a r e 
 F o u n d a t i o n ,   I n c . ,   5 9   T e m p l e   P l a c e   -   S u i t e   3 3 0 ,   B o s t o n ,   M A     0 2 1 1 1 - 1 3 0 7 ,   U S A . 
 	 
   
  
 l     ����  r         n     	    1    	��
�� 
psxp  l     ����  I    ��  
�� .earsffdralis        afdr  m     ��
�� afdrcusr  �� ��
�� 
from  m    ��
�� fldmfldu��  ��  ��    o      ���� 0 
thehomedir 
theHomeDir��  ��        l    ����  r        b        o    ���� 0 
thehomedir 
theHomeDir  m       �   b L i b r a r y / ' A p p l i c a t i o n   S u p p o r t ' / L y X - 2 . 0 / . l y x p i p e . i n  o      ���� 0 thelyxpipe20x theLyxPipe20x��  ��        l     ����   r     ! " ! b     # $ # o    ���� 0 
thehomedir 
theHomeDir $ m     % % � & & b L i b r a r y / ' A p p l i c a t i o n   S u p p o r t ' / L y X - 1 . 6 / . l y x p i p e . i n " o      ���� 0 thelyxpipe16x theLyxPipe16x��  ��     ' ( ' l    )���� ) r     * + * b     , - , o    ���� 0 
thehomedir 
theHomeDir - m     . . � / / b L i b r a r y / ' A p p l i c a t i o n   S u p p o r t ' / L y X - 1 . 5 / . l y x p i p e . i n + o      ���� 0 thelyxpipe15x theLyxPipe15x��  ��   (  0 1 0 l   # 2���� 2 r    # 3 4 3 b    ! 5 6 5 o    ���� 0 
thehomedir 
theHomeDir 6 m      7 7 � 8 8 b L i b r a r y / ' A p p l i c a t i o n   S u p p o r t ' / L y X - 1 . 4 / . l y x p i p e . i n 4 o      ���� 0 thelyxpipe14x theLyxPipe14x��  ��   1  9 : 9 l  $ ) ;���� ; r   $ ) < = < b   $ ' > ? > o   $ %���� 0 
thehomedir 
theHomeDir ? m   % & @ @ � A A Z L i b r a r y / ' A p p l i c a t i o n   S u p p o r t ' / L y X / . l y x p i p e . i n = o      ���� 0 thelyxpipe137 theLyxPipe137��  ��   :  B C B l  * 3 D���� D r   * 3 E F E b   * / G H G o   * +���� 0 
thehomedir 
theHomeDir H m   + . I I � J J  . l y x / l y x p i p e . i n F o      ���� 0 thelyxpipe136 theLyxPipe136��  ��   C  K L K l     ��������  ��  ��   L  M N M l     �� O P��   O K E This checks for the existence of LyX pipes starting from most recent    P � Q Q �   T h i s   c h e c k s   f o r   t h e   e x i s t e n c e   o f   L y X   p i p e s   s t a r t i n g   f r o m   m o s t   r e c e n t N  R S R l     �� T U��   T   versions of LyX.    U � V V "   v e r s i o n s   o f   L y X . S  W X W l  4 E Y���� Y r   4 E Z [ Z I  4 A�� \��
�� .sysoexecTEXT���     TEXT \ b   4 = ] ^ ] b   4 9 _ ` _ m   4 7 a a � b b  t e s t   - p   ` o   7 8���� 0 thelyxpipe20x theLyxPipe20x ^ m   9 < c c � d d    ;   e c h o   $ ?  ��   [ o      ���� 0 v20x  ��  ��   X  e f e l  F W g���� g r   F W h i h I  F S�� j��
�� .sysoexecTEXT���     TEXT j b   F O k l k b   F K m n m m   F I o o � p p  t e s t   - p   n o   I J���� 0 thelyxpipe16x theLyxPipe16x l m   K N q q � r r    ;   e c h o   $ ?  ��   i o      ���� 0 v16x  ��  ��   f  s t s l  X i u���� u r   X i v w v I  X e�� x��
�� .sysoexecTEXT���     TEXT x b   X a y z y b   X ] { | { m   X [ } } � ~ ~  t e s t   - p   | o   [ \���� 0 thelyxpipe15x theLyxPipe15x z m   ] `   � � �    ;   e c h o   $ ?  ��   w o      ���� 0 v15x  ��  ��   t  � � � l  j { ����� � r   j { � � � I  j w�� ���
�� .sysoexecTEXT���     TEXT � b   j s � � � b   j o � � � m   j m � � � � �  t e s t   - p   � o   m n���� 0 thelyxpipe14x theLyxPipe14x � m   o r � � � � �    ;   e c h o   $ ?  ��   � o      ���� 0 v14x  ��  ��   �  � � � l  | � ����� � r   | � � � � I  | ��� ���
�� .sysoexecTEXT���     TEXT � b   | � � � � b   | � � � � m   |  � � � � �  t e s t   - p   � o    ����� 0 thelyxpipe137 theLyxPipe137 � m   � � � � � � �    ;   e c h o   $ ?  ��   � o      ���� 0 v137  ��  ��   �  � � � l  � � ����� � r   � � � � � I  � ��� ���
�� .sysoexecTEXT���     TEXT � b   � � � � � b   � � � � � m   � � � � � � �  t e s t   - p   � o   � ����� 0 thelyxpipe136 theLyxPipe136 � m   � � � � � � �    ;   e c h o   $ ?  ��   � o      ���� 0 v136  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l  � ����� � Z   � � � ��� � l  � � ����� � =   � � � � � o   � ����� 0 v20x   � m   � � � � � � �  0��  ��   � r   � � � � � o   � ����� 0 thelyxpipe20x theLyxPipe20x � o      ���� 0 
thelyxpipe 
theLyxPipe �  � � � l  � � ����� � =   � � � � � o   � ����� 0 v16x   � m   � � � � � � �  0��  ��   �  � � � r   � � � � � o   � ����� 0 thelyxpipe16x theLyxPipe16x � o      ���� 0 
thelyxpipe 
theLyxPipe �  � � � l  � � ����� � =   � � � � � o   � ����� 0 v15x   � m   � � � � � � �  0��  ��   �  � � � r   � � � � � o   � ����� 0 thelyxpipe15x theLyxPipe15x � o      ���� 0 
thelyxpipe 
theLyxPipe �  � � � l  � � ����� � =   � � � � � o   � ����� 0 v14x   � m   � � � � � � �  0��  ��   �  � � � r   � � � � � o   � ����� 0 thelyxpipe14x theLyxPipe14x � o      ���� 0 
thelyxpipe 
theLyxPipe �  � � � l  � � ����� � =   � � � � � o   � ����� 0 v137   � m   � � � � � � �  0��  ��   �  � � � r   � � � � � o   � ����� 0 thelyxpipe137 theLyxPipe137 � o      ���� 0 
thelyxpipe 
theLyxPipe �  � � � l  � ����� � =   � � � � o   � ����� 0 v136   � m   � � � � � �  0��  ��   �  ��� � r   � � � o  	���� 0 thelyxpipe136 theLyxPipe136 � o      ���� 0 
thelyxpipe 
theLyxPipe��  ��  ��  ��   �  � � � l     ��������  ��  ��   �  � � � l     �� � ���   � : 4 Get the selected citations and push to the LyX pipe    � � � � h   G e t   t h e   s e l e c t e d   c i t a t i o n s   a n d   p u s h   t o   t h e   L y X   p i p e �  � � � l � ����� � O  � � � � k  � � �  � � � l �� � ��   � 7 1 Collect the keys of the selected citations as a      � b   C o l l e c t   t h e   k e y s   o f   t h e   s e l e c t e d   c i t a t i o n s   a s   a   �  l ����     comma separated string    � .   c o m m a   s e p a r a t e d   s t r i n g  r  	
	 m   �  
 o      ���� 	0 cites    O   s k  )r  r  )2 1  ).��
�� 
sele o      ���� 0 sel   �� Z  3r��� > 39 o  36�~�~ 0 sel   J  68�}�}   X  <n�| O  Ri r  Vh  b  Vd!"! b  V]#$# o  VY�{�{ 	0 cites  $ m  Y\%% �&&  ," l ]c'�z�y' e  ]c(( 1  ]c�x
�x 
ckey�z  �y    o      �w�w 	0 cites   o  RS�v�v 0 pub  �| 0 pub   o  ?B�u�u 0 sel  ��  �  ��   l  &)�t�s) 4  &�r*
�r 
docu* m  $%�q�q �t  �s   +,+ l tt�p�o�n�p  �o  �n  , -.- l tt�m/0�m  / 2 , Don't do anything if there are no citations   0 �11 X   D o n ' t   d o   a n y t h i n g   i f   t h e r e   a r e   n o   c i t a t i o n s. 2�l2 Z  t�34�k�j3 > t{565 o  tw�i�i 	0 cites  6 m  wz77 �88  4 k  ~�99 :;: l ~~�h<=�h  <   Remove leading comma   = �>> *   R e m o v e   l e a d i n g   c o m m a; ?@? r  ~�ABA l ~�C�g�fC n  ~�DED 7 ���eFG
�e 
ctxtF m  ���d�d G m  ���c�c��E o  ~��b�b 	0 cites  �g  �f  B o      �a�a 	0 cites  @ HIH l ���`�_�^�`  �_  �^  I JKJ l ���]LM�]  L - ' Create the command to send to the pipe   M �NN N   C r e a t e   t h e   c o m m a n d   t o   s e n d   t o   t h e   p i p eK OPO r  ��QRQ m  ��SS �TT > L Y X C M D : B i b D e s k : c i t a t i o n - i n s e r t :R o      �\�\ 0 	thelyxcmd 	theLyxCmdP UVU r  ��WXW l ��Y�[�ZY b  ��Z[Z o  ���Y�Y 0 	thelyxcmd 	theLyxCmd[ o  ���X�X 	0 cites  �[  �Z  X o      �W�W 0 	thelyxcmd 	theLyxCmdV \]\ l ���V�U�T�V  �U  �T  ] ^_^ l ���S`a�S  ` 5 / Send the command to the current user's lyxpipe   a �bb ^   S e n d   t h e   c o m m a n d   t o   t h e   c u r r e n t   u s e r ' s   l y x p i p e_ c�Rc I ���Qd�P
�Q .sysoexecTEXT���     TEXTd b  ��efe b  ��ghg b  ��iji m  ��kk �ll 
 e c h o  j o  ���O�O 0 	thelyxcmd 	theLyxCmdh m  ��mm �nn    >  f o  ���N�N 0 
thelyxpipe 
theLyxPipe�P  �R  �k  �j  �l   � m  oo�                                                                                  BDSK   alis    T  Macintosh HD               �x��H+  �nBibDesk.app                                                    '��NQ�        ����  	                TeX     �yE?      �N�(    �n  <  )Macintosh HD:Applications:TeX:BibDesk.app     B i b D e s k . a p p    M a c i n t o s h   H D  Applications/TeX/BibDesk.app  / ��  ��  ��   � p�Mp l     �L�K�J�L  �K  �J  �M       �Iqr�I  q �H
�H .aevtoappnull  �   � ****r �Gs�F�Etu�D
�G .aevtoappnull  �   � ****s k    �vv  
ww  xx  yy  'zz  0{{  9||  B}}  W~~  e  s��  ���  ���  ���  ���  ��C�C  �F  �E  t �B�B 0 pub  u =�A�@�?�>�=�< �; %�: .�9 7�8 @�7 I�6 a c�5�4 o q�3 } �2 � ��1 � ��0 � ��/ ��. � � � � �o�-�,�+�*�)�(�'%�&7�%S�$km
�A afdrcusr
�@ 
from
�? fldmfldu
�> .earsffdralis        afdr
�= 
psxp�< 0 
thehomedir 
theHomeDir�; 0 thelyxpipe20x theLyxPipe20x�: 0 thelyxpipe16x theLyxPipe16x�9 0 thelyxpipe15x theLyxPipe15x�8 0 thelyxpipe14x theLyxPipe14x�7 0 thelyxpipe137 theLyxPipe137�6 0 thelyxpipe136 theLyxPipe136
�5 .sysoexecTEXT���     TEXT�4 0 v20x  �3 0 v16x  �2 0 v15x  �1 0 v14x  �0 0 v137  �/ 0 v136  �. 0 
thelyxpipe 
theLyxPipe�- 	0 cites  
�, 
docu
�+ 
sele�* 0 sel  
�) 
kocl
�( 
cobj
�' .corecnte****       ****
�& 
ckey
�% 
ctxt�$ 0 	thelyxcmd 	theLyxCmd�D����l �,E�O��%E�O��%E�O��%E�O��%E�O��%E�O�a %E` Oa �%a %j E` Oa �%a %j E` Oa �%a %j E` Oa �%a %j E` Oa �%a  %j E` !Oa "_ %a #%j E` $O_ a %  
�E` &Y __ a '  
�E` &Y M_ a (  
�E` &Y ;_ a )  
�E` &Y )_ !a *  
�E` &Y _ $a +  _ E` &Y hOa , �a -E` .O*a /k/ K*a 0,E` 1O_ 1jv 7 1_ 1[a 2a 3l 4kh  � _ .a 5%*a 6,E%E` .U[OY��Y hUO_ .a 7 @_ .[a 8\[Zl\Zi2E` .Oa 9E` :O_ :_ .%E` :Oa ;_ :%a <%_ &%j Y hU ascr  ��ޭ