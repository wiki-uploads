#LyX 1.5.3 created this file. For more info see http://www.lyx.org/
\lyxformat 276
\begin_document
\begin_header
\textclass article
\language english
\inputencoding auto
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\paperfontsize default
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Standard
\begin_inset Formula \[
v_{k}=\begin{cases}
k(b-k) & p=q\\
\frac{b}{p-q}\frac{1-\left(\frac{q}{p}\right)^{k}}{1-\left(\frac{q}{p}\right)^{b}}-\frac{k}{p-q} & p\neq q\end{cases}\]

\end_inset


\newline

\end_layout

\begin_layout Standard
\begin_inset Formula \[
P^{n}=\left(\begin{array}{cc}
1 & -\alpha\\
1 & \beta\end{array}\right)\left(\begin{array}{cc}
1 & 0\\
0 & (1-\alpha-\beta)^{n}\end{array}\right)\left(\begin{array}{cc}
1 & -\alpha\\
1 & \beta\end{array}\right)\]

\end_inset


\newline

\newline

\newline

\end_layout

\begin_layout Standard
\begin_inset Formula \[
\sum_{j=0}^{\infty}\theta_{j}=\sum_{j=0}^{s-1}\frac{1}{j!}\left(\frac{\lambda}{\mu}\right)^{j}+\sum_{j=s}^{\infty}\frac{1}{s!}\left(\frac{\lambda}{\mu}\right)^{s}\left(\frac{\lambda}{s\mu}\right)^{j-s}\]

\end_inset


\end_layout

\begin_layout Standard
The traffic intensity in an 
\begin_inset Formula $M/M/s$
\end_inset

 system is 
\begin_inset Formula $\rho=\lambda/(s\mu)$
\end_inset

.
 As the intensity of traffic approaches one, the mean queue length becomes
 unbounded.
 When 
\begin_inset Formula $\lambda<s\mu$
\end_inset

, then from (1.8) and (1.15), 
\begin_inset Formula \[
\pi_{0}=\left\{ \sum_{j=0}^{s-1}\frac{1}{j!}\left(\frac{\lambda}{\mu}\right)^{j}+\frac{(\lambda/\mu)^{s}}{s!(1-\lambda/s\mu)}\right\} ^{-1}\]

\end_inset

and
\newline

\begin_inset Formula \[
\pi_{k}=\begin{cases}
\frac{1}{k!}\left(\frac{\lambda}{\mu}\right)^{k}\pi_{0} & \text{for\,}k=0,1,\ldots,s,\\
\frac{1}{s!}\left(\frac{\lambda}{\mu}\right)^{s}\left(\frac{\lambda}{s\mu}\right)^{k-s}\pi_{0} & \text{for}\, k\geq s\end{cases}\]

\end_inset


\newline

\newline

\newline

\begin_inset Float table
wide false
sideways false
status open

\begin_layout Standard
\begin_inset Caption

\begin_layout Standard
Running times (seconds)
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="4">
<features>
<column alignment="center" valignment="top" leftline="true" width="0">
<column alignment="center" valignment="top" leftline="true" width="0">
<column alignment="center" valignment="top" leftline="true" width="0">
<column alignment="center" valignment="top" leftline="true" rightline="true" width="0">
<row topline="true" bottomline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
Case
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
Method 1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
Method 2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
Method 3
\end_layout

\end_inset
</cell>
</row>
<row topline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
23
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
47
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
389
\end_layout

\end_inset
</cell>
</row>
<row topline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
43
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
53
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
393
\end_layout

\end_inset
</cell>
</row>
<row topline="true" bottomline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
96
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
87
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Standard
426
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Standard

\end_layout

\end_inset


\end_layout

\end_body
\end_document
