# Standard textclass definition file. Taken from initial LyX source code
# Author : Matthias Ettrich <ettrich@informatik.uni-tuebingen.de>
# Transposed by Pascal Andr� <andre@via.ecp.fr>
# Heavily modifed and enhanced by several developers.
# Split from stdstruct.inc by J�rgen Spitzm�ller <j.spitzmueller@gmx.de>

# This include special environments for linguistic papers,
# like those provided by covington.sty.


# single numbered example with covington.sty
Style Numbered_Example_Single
	LatexType       Environment
	LatexName       example
	NextNoIndent    1
	LeftMargin      Example:
	LabelSep        xx
	ParSkip               0.0
	ItemSep               0.2
	TopSep                0.7
	BottomSep             0.7
	ParSep                0.3
	Align                 Block
	AlignPossible         Block, Left
	LabelType             Static
	LabelString           "Example:"
	LabelFont
		Shape           Italic
		Color           Blue
	EndFont
	Preamble
		\usepackage{covington}
	EndPreamble
End


# multiple numbered example with covington.sty
Style Numbered_Examples
	CopyStyle	      Numbered_Example_Single
	Margin                Static
	LatexType             Item_Environment
	LatexName             examples
End

# multiple numbered example with covington.sty
Style Subexample
	CopyStyle	      Numbered_Example_Single
	Margin                Static
	LatexType             Item_Environment
	LatexName             subexample
	LeftMargin            Subexample:
	LabelString           "Subexample:"
	Preamble
		\usepackage{covington}
		\newenvironment{subexample}{%
			\begin{example}\begin{enumerate}
			\renewcommand\theenumi{\alph{enumi}}
			\renewcommand\labelenumi{(\theenumi)}
			\renewcommand\p@enumi{\theequation\,}}%
			{\end{enumerate}\end{example}}
	EndPreamble
End
