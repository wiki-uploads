/* tgif-fixepsi - swaps black and white in the image preview of an EPSI file
 *                such that tgif displays it correctly
 *
 * Author: Enrico Forestieri
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int ac, char **av)
{
    FILE *fdi, *fdo;
    char buf[1024];
    char tname[1024];
    char *s;
    int foundpreview = 0;

    if (ac != 2)
    {
	fprintf(stderr, "Usage: tgif-fixepsi file.epsi\n");
	exit(0);
    }

    if (!(fdi = fopen(av[1],"r")))
    {
	fprintf(stderr, "tgif-fixepsi: cannot open %s\n", av[1]);
	exit(1);
    }

    strcpy(tname,av[1]);
    strcat(tname,".tmp");
    if (!(fdo = fopen(tname,"w")))
    {
	fprintf(stderr, "tgif-fixepsi: cannot open temporary file for writing\n");
	fclose(fdi);
	exit(1);
    }

    if (fgets(buf,sizeof(buf),fdi))
    {
	fputs(buf,fdo);
	if (strncmp(buf,"%!PS",4))
	{
	    fprintf(stderr,"tgif-fixepsi: %s is not a postscript file\n",av[1]);
	    fclose(fdi);
	    fclose(fdo);
	    unlink(tname);
	    exit(1);
	}
    }
    else
    {
	fclose(fdi);
	fclose(fdo);
	unlink(tname);
	exit(0);
    }

    while (fgets(buf,sizeof(buf),fdi))
    {
	fputs(buf,fdo);
	if (!strncmp(buf,"%%BeginPreview:",15))
	{
	    foundpreview = 1;
	    break;
	}
    }

    if (!foundpreview)
    {
	fprintf(stderr,"tgif-fixepsi: no preview bitmap in %s\n",av[1]);
	fclose(fdi);
	fclose(fdo);
	unlink(tname);
	exit(0);
    }

    while (fgets(buf,sizeof(buf),fdi))
    {
	if (!strncmp(buf,"%%EndPreview",12))
	{
	    fputs(buf,fdo);
	    break;
	}
	for (s=buf; *s; s++)
	{
	    switch (*s)
	    {
		case '0':
		    *s = 'F';
		    break;
		case '1':
		    *s = 'E';
		    break;
		case '2':
		    *s = 'D';
		    break;
		case '3':
		    *s = 'C';
		    break;
		case '4':
		    *s = 'B';
		    break;
		case '5':
		    *s = 'A';
		    break;
		case '6':
		    *s = '9';
		    break;
		case '7':
		    *s = '8';
		    break;
		case '8':
		    *s = '7';
		    break;
		case '9':
		    *s = '6';
		    break;
		case 'A':
		    *s = '5';
		    break;
		case 'B':
		    *s = '4';
		    break;
		case 'C':
		    *s = '3';
		    break;
		case 'D':
		    *s = '2';
		    break;
		case 'E':
		    *s = '1';
		    break;
		case 'F':
		    *s = '0';
		    break;
	    }
	}
	fputs(buf,fdo);
    }

    while (fgets(buf,sizeof(buf),fdi))
    {
	fputs(buf,fdo);
    }

    fclose(fdi);
    fclose(fdo);
    unlink(av[1]);
    if (rename(tname,av[1]) < 0)
    {
	fprintf(stderr,"fixepsi: cannot rename %s as %s\n",tname,av[1]);
	exit(1);
    }
    return 0;
}
