#!/usr/bin/python3
# -*- coding: utf-8 -*-

# file gnuplot2pstex.py

# author Tobias Hilbricht

# This script converts a gnuplot file to two files that can be processed
# with latex into high quality PDF. It requires gnuplot.
# The extension of the gnuplot file has to be .gp as in gnuplotfile.gp
# Data have to be provided in gnuplotfile.gp in "heredocs". See e. g.
# Philipp Janert, "Gnuplot in Action Second Edition" page 72

# Usage:
#   python gnuplot2pstex.py gnuplotfile.gp
# This command generates
#   1. gnuplotfile.eps     -- the converted EPS file (text stripped)
#   2. gnuplotfile.eps_tex -- a TeX file that can be included in your
#                             LaTeX document using '\input{gnuplotfile.eps_tex}'

import os, sys

# We expect one arg: the name of the gnuplotfile

if len(sys.argv) != 2:
    print("Usage: python gnuplot2pstex.py gnuplotfile.gp")

gnuplot_file = sys.argv[1]
# Strip the extension from ${gnuplotfile}
base_name = os.path.splitext(os.path.basename(gnuplot_file))[0]
# Three letter file extension of TeX file necessary for gnuplot
gnuplot_name = base_name + ".etx"
# TeX file extension known to LyX
lyx_name = base_name + ".eps_tex"

# Call gnuplot    
os.system(f"gnuplot -e \"set term cairolatex eps ; set output '{gnuplot_name}'\" {gnuplot_file}")
# Change TeX file extension 
os.rename(gnuplot_name, lyx_name)
