# Common input file for leo's document classes
# Hellmut Weber <mail@hellmutweber.de>, 2007-06-01 
#
# based on /usr/share/lyx/layouts/scrartcl.layout
# Bernd Rellermeyer <100.41728@germanynet.de>, 1998/7/11.

# Remark the python like indentation ;-)

# General textclass parameters
Input /usr/local/share/lyx150/layouts/scrclass.inc

## NoStyle Chapter
## NoStyle Chapter*
## NoStyle Addchap
## NoStyle Addchap*
## NoStyle Addpart
## NoStyle Dictum

MaxCounter              Counter_Section
SecNumDepth             5
TocDepth                5

# Modify Part
Style Part
  Align                 Left
  AlignPossible         Left
  TopSep                2
  BottomSep             1.5
  Font
    Size                Larger
    EndFont
  End

# Modify Part*
Style Part*
  Align                 Left
  AlignPossible         Left
  TopSep                2
  BottomSep             1.5
  Font
    Size                Larger
    EndFont
  End

# New Environment Story (cf. Steve Litt, LyX Quickstart)
Style Story
  LatexType             Environment
  LatexName             story1
  PARSEP                0.7
  Font
    Shape               Italic
    EndFont

  Preamble
    % --- Start Preambel part for Story ---
    \let\oldparindent=\parindent
    \newenvironment{story1} {
      \setlength{\parindent}{0.0pt}
      \itshape
      }
    {
      \par
      \vspace{0.5\baselineskip}
      \setlength{\parindent}{\oldparindent}
      }
    % --- End   Preambel part for Story ---
    EndPreamble
  End

# New Environment Short_Enum
Style Enumshort
  LatexType             Item_Environment
  LatexName             enumshort
  NextNoIndent          1
  LeftMargin            MMN
  LabelSep              xx
  ItemSep               0.2
  TopSep                0.7
  BottomSep             0.7
  ParSep                0.3
  Align                 Block
  AlignPossible         Block, Left
  LabelType             Counter_EnumI

  Preamble
    % --- Start Preambel part for Story ---
    % \RequirePackage{setspace} # cf. ctcpaper.sty / ctcartkl.sty ???
    \let\olditemsep=\itemsep
    \newenvironment{enumshort}{%
      \vspace{-2.0ex}%
      \begin{enumerate}%
      \begin{spacing}{0.85}%
      \setlength{\itemsep}{-0.5ex}%
      }%
    {%
      \let\itemsep=\olditemsep%
      \end{spacing}%
      \end{enumerate}%
      \vspace{-3.0ex}%
      }%
    % --- End   Preambel part for Story ---
    EndPreamble
  End

# New Environment Short_Item
Style Itemshort
  LatexType              Item_Environment
  LatexName              itemshort
  NextNoIndent           1
  LeftMargin             MMN
  LabelSep               xx
  ItemSep                0.2
  TopSep                 0.7
  BottomSep              0.7
  ParSep                 0.3
  Align                  Block
  AlignPossible          Block, Left
  LabelType              Static
  LabelString            *

  Preamble
    % --- Start Preambel part for Itemshort ---
    % \RequirePackage{pifont}
    % Benötigtes Paket 'setspace' s. enumshort
    \let\olditemsep=\itemsep
    \let\oldbaselinestretch=\baselinestretch
    \newenvironment{itemshort} {
      % \vspace*{-0.5ex}
      \begin{itemize}
      \begin{spacing}{0.78}
      \setlength{\itemsep}{0.0ex}
    % \renewcommand{\baselinestretch}{0.80\oldbaselinestretch}
    % \renewcommand{\baselinestretch}{0.80}
    % \normal
      }
    {
      \let\itemsep=\olditemsep
      \let\baselinestretch=\oldbaselinestretch
      \end{spacing}
      \end{itemize}
      \vspace{-2.0ex}
      }
    % --- End   Preambel part for Itemshort ---
    EndPreamble
  End

# New Environment Short_Paragraph
Style Paragraphshort
  CopyStyle              Paragraph
  LatexType              Command
  LatexName              paragraphshort
  TopSep                 0.2

  Preamble
    % --- Start Preambel part for Short_Paragraph ---
    \newcommand{\paragraphshort}[1]{
      \vspace{-1.0ex}
      \paragraph{#1}
      }
    % --- End   Preambel part for Short_Paragraph ---
    EndPreamble
  End

# Jürgen Spitzmüller, lyx-users@lists.lyx.org
# answer to a question of mine
Style Enumerate_Resume
  Margin                Static
  LatexType             Item_Environment
  LatexName             resumeenumerate
  NextNoIndent          1
  LeftMargin            MMN
  LabelSep              xx
  ParSkip               0.0
  ItemSep               0.2
  TopSep                0.7
  BottomSep             0.7
  ParSep                0.3
  Align                 Block
  AlignPossible         Block, Left
  LabelType             Enumerate

  Preamble
    % --- Start Preambel part for Enumerate_Resume ---
    \usepackage{enumitem}
    \newenvironment{resumeenumerate}{%
    \begin{enumerate}[resume]}{\end{enumerate}}
    % --- End   Preambel part for Enumerate_Resume ---
    EndPreamble
  End

# 2007-04-29 Hw, frame-boxed code-parts
# Jürgen Spitzmüller answer to a question of mine
# CharStyle Fboxed_Code, ,-( # Here no underscore!
CharStyle Fboxed
  LatexType             Command
  LatexName             fboxedcode
  LabelFont
    Family              Roman
    Color               blue
    EndFont
  Preamble
    % --- Start Preambel part for Char Style Fboxed ---
    \newcommand{\fboxedcode}[1]{
      \fbox{\texttt{\small%
        #1%
        } }% texttt fbox
      }
    % --- End   Preambel part for Char Style Fboxed ---
    EndPreamble
  End

