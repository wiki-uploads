#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass article
\begin_preamble
\usepackage{hyperref}
\end_preamble
\language english
\inputencoding auto
\fontscheme times
\graphics default
\paperfontsize default
\spacing single 
\papersize Default
\paperpackage widemarginsa4
\use_geometry 0
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle empty

\layout Title

Simple example of using BibTeX
\layout Author

Christian Ridderström
\layout Abstract

This document illustrates a small example of using BibTeX.
 It is basically intended for a novice to download and see that BibTeX works
 on his machine.
 
\series bold 
Note
\series default 
: Both this file (
\emph on 
plainnat.lyx
\emph default 
) and the file with BibTeX references (
\emph on 
references.bib
\emph default 
) are needed, so download both and put them in the same directory.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
thispagestyle{empty}
\end_inset 


\layout Section

Instructions
\layout Standard

In order to use BibTeX, you need to have a database with references (a 
\emph on 
BibTeX-file
\emph default 
, typically with the extension 
\emph on 
.bib
\emph default 
).
 This example comes with such a file already, 
\emph on 
references.bib
\emph default 
, that you should have put in the same directory as this file (
\emph on 
plainnat.lyx
\emph default 
).
 If you did the above, you should be able to view this example by using
 the menu: 
\bar under 
V
\bar default 
iew
\family sans 
\SpecialChar \menuseparator

\bar under 
D
\bar default 
VI.
\layout Section

Creating this example
\layout Standard

Here are the instructions that were followed in order to create the citations
 in this file:
\layout Enumerate

To insert a 
\series bold 
BibTeX-inset
\series default 
, i.e.
 the thing that will appear as your list of references in the end, first
 move the cursor to where you want the bibliography and then use the menu:
 
\bar under 
I
\bar default 
nsert
\family sans 
\SpecialChar \menuseparator

\family default 
Lists\SpecialChar ~
&\SpecialChar ~
T
\bar under 
O
\bar default 
C
\family sans 
\SpecialChar \menuseparator

\family default 
\bar under 
B
\bar default 
ibTeX\SpecialChar ~
Reference.
\begin_deeper 
\layout Standard

A dialog should immediately pop up with the title 
\emph on 
BibTeX
\emph default 
.
 Now you press the button 
\series bold 
Browse
\series default 
 and select the file 
\emph on 
references.bib
\emph default 
.
 Then press 
\series bold 
Add
\series default 
 and the file should appear in the list of databases.
 
\layout Standard

The 
\emph on 
style
\emph default 
 used in this document is 
\emph on 
plainnat
\emph default 
, so choose that at the bottom of the dialog and finally press 
\series bold 
OK
\series default 
 to close the dialog.
\end_deeper 
\layout Enumerate

To insert an actual citation, i.e.
 the thing that often appear as [1], move the cursor to where you want it
 and then use the menu: 
\bar under 
I
\bar default 
nsert
\family sans 
\SpecialChar \menuseparator

\bar under 
C
\bar default 
itation\SpecialChar ~
Reference.
\begin_deeper 
\layout Standard

or simply press 
\series bold 
M-i c
\series default 
.
 This should pop up another dialog called 
\emph on 
Citation
\emph default 
 that contains a list of available references.
 In particular, you should see an entry with the 
\emph on 
BibTeX citation key
\emph default 
 Ridderstrom:2003:LLC.
 Double-click on that entry to add it to the list of selected references,
 and then close the dialog.
 This should insert a 
\emph on 
citation-inset
\emph default 
, like this one: 
\begin_inset LatexCommand \cite{Ridderstrom:2003:LLC}

\end_inset 

.
\end_deeper 
\layout Standard

That was really it and if your system is properly configured, just run 
\bar under 
V
\bar default 
iew
\family sans 
\SpecialChar \menuseparator

\bar under 
D
\bar default 
VI
\family default 
 to see the result.
\layout Standard


\begin_inset LatexCommand \BibTeX[plainnat]{references}

\end_inset 


\the_end
