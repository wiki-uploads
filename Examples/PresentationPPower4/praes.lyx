#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass foils
\begin_preamble
\usepackage{pause}
\usepackage{background}
\usepackage[pdftex]{color}
\usepackage{hyperref}
\usepackage{pp4slide}
\hypersetup{%
  pdfpagemode={FullScreen},
  urlcolor=yellow,
  pdfauthor={Dominik Wa�enhoven},
  pdftitle={pdf-Pr�sentationen mit LyX}
} 
\usepackage[pdftex]{geometry} 
\geometry{headsep=5ex,hscale=0.9} 
\definecolor{dkblue}{rgb}{0,0.1,0.5} 
\definecolor{bblue}{rgb}{0.75,0.75,1} 
\leftheader{%
  \color{white}%
  \scriptsize%
  \fontfamily{bch}%
  \selectfont pdf-Pr�sentationen mit \LyX
}
\rightheader{%
  \color{white}%
  \scriptsize%
  \fontfamily{bch}%
  \selectfont \thepage
}
\usepackage[ngerman]{babel}
\end_preamble
\language ngerman
\inputencoding auto
\fontscheme default
\graphics default
\paperfontsize default
\spacing single 
\papersize Default
\paperpackage a4
\use_geometry 0
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation landscape
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 4
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language german
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pagecolor{dkblue}
\end_inset 


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
hypersetup{pdfpagetransition={R},pdfpagemode={FullScreen}}
\end_inset 


\layout Title


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}{
\backslash 
fontfamily{pzc}
\backslash 
selectfont pdf-Pr�sentationen mit}
\end_inset 


\newline 
LyX
\layout Author
\added_space_top bigskip 
Dominik Wa�enhoven
\layout Date

2.
 November 2003
\layout Foilhead

Wie erstelle ich eine Pr�sentation?
\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard
\added_space_top bigskip \align center 

\size large 
\SpecialChar \ldots{}
 und zwar unter 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\size large 
Linux
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 


\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard
\added_space_top bigskip \align center 

\size large 
f�r den 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\size large 
Acrobat
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{
\backslash 
textregistered{}}
\end_inset 

 Reader
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 


\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard
\added_space_top bigskip \align center 

\size large 
mit 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\size large 
LyX
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 


\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 

, 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\size large 
LaTeX
\size default 
 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 


\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause{}
\end_inset 

 und 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\size large 
P
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 

 (PPower4)
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 


\layout Foilhead

LyX-Einstellungen
\layout Itemize

Unter 
\emph on 
Layout\SpecialChar \menuseparator
Dokument
\emph default 
 muss die Klasse 
\emph on 
slides (FoilTe
\begin_inset ERT
status Collapsed

\layout Standard
{
\end_inset 

X
\begin_inset ERT
status Collapsed

\layout Standard
}
\end_inset 

)
\emph default 
 gew�hlt werden
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\begin_deeper 
\layout Itemize

Die Standardschriftgr��e ist 20pt
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 

, Alternativen sind 17pt, 15pt und 30pt.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\end_deeper 
\layout Itemize

Zus�tzlich m�ssen einige Pakete in der Pr�ambel eingebunden werden:
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring MMMMMMMMMMMMMMMMM


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size footnotesize 

\backslash 
usepackage[pdftex]{color}
\family default 
 f�r einfache und automatische Farbeinstellungen
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring MMMMMMMMMMMMMMMMM


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size footnotesize 

\backslash 
usepackage{hyperref}
\family default 
 f�r Hyperlinks
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring MMMMMMMMMMMMMMMMM


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size footnotesize 

\backslash 
usepackage{pause}
\family default 
 f�r die Verz�gerungseffekte
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring MMMMMMMMMMMMMMMMM


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size footnotesize 

\backslash 
usepackage{background}
\family default 
 f�r die Hintergrundfarben
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring MMMMMMMMMMMMMMMMM


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size footnotesize 

\backslash 
usepackage{pp4slide}
\family default 
 f�r die Seiten�berg�nge
\layout Foilhead

LaTeX-Befehle f�r P
\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 


\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

LyX 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

 bietet einige M�glichkeiten an, um die 
\emph on 
slides
\emph default 
-Befehle zu nutzen, so z.
\begin_inset Formula $\,$
\end_inset 

B.
 
\emph on 
foilhead
\emph default 
 f�r die �berschrift einer neuen Folie.
 Dennoch m�ssen einige Kommandos als ERT (Evil Red Text) eingegeben werden.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause{}
\end_inset 


\newline 
Die wichtigsten davon sind:
\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size small 

\backslash 
pause
\family default 
 An dieser Stelle wird die Pr�sentation angehalten und erst nach Tastendruck
 fortgesetzt.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size small 

\backslash 
hypersetup{pdfpagetransition=R}
\family default 
 Die folgende Folie und alle Elemente auf ihr werden bei Tastendruck direkt
 eingeblendet.
 (Das ist die Standard-Einstellung.)
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 


\family typewriter 
\size small 

\backslash 
hypersetup{pdfpagetransition={Dissolve}}
\family default 
 Nun werden die Elemente allm�hlich eingeblendet (so ab der n�chsten Seite).
\layout Foilhead


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 

LaTeX-Befehle f�r P
\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 


\size default 
 (Forts.)
\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
hypersetup{pdfpagetransition={Dissolve}}
\end_inset 


\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pagecolor{yellow}
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
pagecolor{yellow} 
\family default 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
Die Standard-Hintergrundfarbe ist blau.
 Hier wird sie nun zu gelb ge�ndert.
 Dabei sollte man auch die Schriftfarben �ndern, z.
\begin_inset Formula $\,$
\end_inset 

B.
 mit 
\family typewriter 

\backslash 
color{red}
\family default 
.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
definecolor{dkblue}{rgb}{0,0.1,0.5} 
\family default 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
Man kann auch eigene Farben definieren, so z.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
,
\end_inset 

B.
 sieht die Definition f�r die Hintergrundfarbe der restlichen Seiten aus,
 die gleichzeitig auch die Schriftfarbe auf der aktuellen Seite ist.
\layout Foilhead


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 

LaTeX-Befehle f�r P
\size large 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 


\size default 
 (Forts.)
\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 

Und noch mehr \SpecialChar \ldots{}

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
pauseVOSplit
\family default 
\SpecialChar ~
und
\family typewriter 
\SpecialChar ~

\backslash 
pauseVISplit 
\family default 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
Auch �berg�nge innerhalb einer Seite lassen sich unterschiedlich gestalten
\size default 
.
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pauseVOSplit
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
pauseVBlinds 
\family default 
und
\family typewriter 
 
\backslash 
pauseHBlinds 
\family default 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
So zum Beispiel \SpecialChar \ldots{}

\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pauseHBlinds
\end_inset 


\layout List
\labelwidthstring 00.00.0000


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
pauseReplace 
\family default 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
schaltet wieder auf 
\begin_inset Quotes gld
\end_inset 

normal
\begin_inset Quotes grd
\end_inset 

 um.
 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pauseReplace
\end_inset 

 Ein einfaches 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 


\family typewriter 
\size small 

\backslash 
pause
\family default 
 
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 


\size small 
wiederholt �brigens den zuletzt gew�hlten �bergangs-Modus.
\size default 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard

Nat�rlich gibt es noch mehr M�glichkeiten -- nachzulesen in der 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{dkblue}
\end_inset 

P
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{red}
\end_inset 

-Anleitung.
\layout Foilhead

Post-Processing mit P
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
textsuperscript{4}
\end_inset 


\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
hypersetup{pdfpagetransition=R}
\end_inset 


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pagecolor{dkblue}
\end_inset 


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

Unter 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

Linux 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

muss man nun einfach die 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

LyX
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

-Datei z.
\begin_inset Formula $\,$
\end_inset 

B.
 als 
\family typewriter 
praes.pdf
\family default 
 exportieren und die entstandene PDF-Datei mit folgendem Befehl bearbeiten:
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard


\family typewriter 
ppower4 praes.pdf praesentation.pdf
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard
\added_space_top bigskip 
Noch einfacher geht es mit 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

tex2pdf
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

, einem Perl-Script, das auf 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

pdflatex 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

basiert.
 Bei 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

tex2pdf 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

kann man die Nachbearbeitung mittels 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

PPower4 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

in den Einstellungen angeben und so einfach aus 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{yellow}
\end_inset 

LyX 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
color{white}
\end_inset 

heraus starten.
\layout Standard

Das war's schon \SpecialChar \ldots{}

\layout Foilhead

Quellenangaben
\layout List
\labelwidthstring 00.00.0000

\SpecialChar ~
 
\begin_inset Formula $\circ$
\end_inset 

 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://www.adobe.com}{Acrobat Reader}
\end_inset 


\layout List
\labelwidthstring 00.00.0000

\SpecialChar ~
 
\begin_inset Formula $\circ$
\end_inset 

 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://www.dante.de}{
\backslash 
LaTeX{} (DANTE)}
\end_inset 


\layout List
\labelwidthstring 00.00.0000

\SpecialChar ~
 
\begin_inset Formula $\circ$
\end_inset 

 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://www.lyx.org}{
\backslash 
LyX}
\end_inset 


\layout List
\labelwidthstring 00.00.0000

\SpecialChar ~
 
\begin_inset Formula $\circ$
\end_inset 

 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://www-sp.iti.informatik.tu-darmstadt.de/software/ppower4/}{PPower4}
\end_inset 


\layout List
\labelwidthstring 00.00.0000

\SpecialChar ~
 
\begin_inset Formula $\circ$
\end_inset 

 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://tex2pdf.berlios.de}{tex2pdf}
\end_inset 


\layout Foilhead

Impressum
\layout Standard
\align center 
Autor: Dominik Wa�enhoven
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
pause
\end_inset 


\layout Standard
\added_space_top 1cm 
Ich habe dieses Dokument entworfen, als ich mich selbst damit besch�ftigt
 habe, eine Pr�sentation zu erstellen.
 Es ist nur als erster Einstieg gedacht und alles andere als umfassend.
 Anregungen aller Art, Verbesserungsvorschl�ge und (hoffentlich keine) Hinweise
 auf Fehler meinerseits sind ausdr�cklich erw�nscht.
\layout Standard
\added_space_top 1cm \align center 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{mailto:domwass@web.de}{domwass@web.de}
\end_inset 


\layout Standard
\align center 
Meine 
\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
href{http://www.wassenhoven.info/dominik/}{Homepage}
\end_inset 


\the_end
