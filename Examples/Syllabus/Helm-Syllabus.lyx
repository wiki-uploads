#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass article
\begin_preamble
\usepackage[%
%	colorlinks=false,%
pagebackref%
]{hyperref}

\date{}
\end_preamble
\language american
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize default
\spacing single 
\papersize Default
\paperpackage a4
\use_geometry 1
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\leftmargin 1.5in
\topmargin 1.5in
\rightmargin 1.5in
\bottommargin 1.5in
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle headings

\layout Title
\noindent 

\series bold 

\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
vspace{-.75in}
\end_inset 


\newline 
Philosophy of Mind
\newline 
PHI/SPM 250
\layout Author
\noindent 
Bennett Helm
\layout Standard
\added_space_top -0.6in \align center 

\size normal 

\begin_inset  Tabular
<lyxtabular version="3" rows="1" columns="2">
<features>
<column alignment="block" valignment="top" width="2in">
<column alignment="block" valignment="top" width="2in">
<row>
<cell alignment="left" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard
\align center 
Stager 323
\newline 
x4392
\newline 
bennett.helm-dIVErvDMbp+HXe+LvDLADg@public.gmane.org
\end_inset 
</cell>
<cell alignment="left" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard
\align center 

\bar under 
Office Hours
\bar default 

\newline 
T: 3--4:00; W: 10--11:30; F: 8:30--10:00,
\newline 
and by appointment
\begin_inset Note
collapsed false

\layout Standard

Change this!!!!
\end_inset 


\end_inset 
</cell>
</row>
</lyxtabular>

\end_inset 


\layout Section

Overview
\layout Standard

Blah, blah \SpecialChar \ldots{}

\layout Section

Required Text
\layout Standard

The required text for this course is:
\layout Itemize

Rosenthal, 
\emph on 
The Nature of Mind
\emph default 
 (Oxford, 1991).
 
\begin_inset Note
collapsed false

\layout Standard

Change this!!!!
\end_inset 


\layout Standard

There will also be many readings available on my eDisk as .PDF files.
\layout Standard
\added_space_top smallskip \line_top \noindent 

\series bold 
\size large 
Note on Copyright Restrictions
\layout Standard
\added_space_bottom smallskip \line_bottom 
All .PDF files I am making available for this course are used in accordance
 with �110 of the Copyright Act of 1976.
 This permits \SpecialChar \ldots{}

\layout Section

Course Requirements
\layout Standard

My aim is \SpecialChar \ldots{}

\layout Standard
\added_space_top medskip \added_space_bottom medskip \noindent \align center 

\begin_inset  Tabular
<lyxtabular version="3" rows="5" columns="2">
<features>
<column alignment="left" valignment="top" width="0sp">
<column alignment="center" valignment="top" width="0sp">
<row bottomline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard


\series bold 
Requirement
\end_inset 
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard


\series bold 
% of Grade
\end_inset 
</cell>
</row>
<row topline="true">
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard

1.
 Exam #1
\end_inset 
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard

25%
\end_inset 
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard

2.
 Exam #2
\end_inset 
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard

25%
\end_inset 
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard

3.
 Research Paper
\end_inset 
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard

30%
\end_inset 
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\layout Standard

4.
 Class Participation
\end_inset 
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\layout Standard

20%
\end_inset 
</cell>
</row>
</lyxtabular>

\end_inset 


\layout Subsection

Exams
\layout Standard

We will have two exams in this course (scheduled for March 9th and April
 20th
\begin_inset Note
collapsed false

\layout Standard

Change this!!!!
\end_inset 

).
 \SpecialChar \ldots{}

\layout Subsection

Research Paper
\layout Standard

\SpecialChar \ldots{}

\layout Subsection

Class Participation
\layout Standard

\SpecialChar \ldots{}

\layout Section

Schedule of Readings
\layout Description

1/19 Introduction.
 (Read and discuss Hooper, 
\begin_inset Quotes eld
\end_inset 

Are We Puppets or Free Agents?
\begin_inset Quotes erd
\end_inset 

) (Handout)
\layout Description

1/24 Introduction: How to do philosophy.
 Read:
\begin_deeper 
\layout Enumerate

Rosenberg, 
\begin_inset Quotes eld
\end_inset 

Philosophy in Action: A Case Study,
\begin_inset Quotes erd
\end_inset 

 Chapter 2 of 
\emph on 
The Practice of Philosophy
\emph default 
 (Prentice Hall, 1996), 10--19.
 (.PDF file.)
\layout Enumerate

Rosenberg, 
\begin_inset Quotes eld
\end_inset 

Engaging the Argument: Content,
\begin_inset Quotes erd
\end_inset 

 Chapter 5 of 
\emph on 
The Practice of Philosophy
\emph default 
 (Prentice Hall, 1996), 43--53.
 (.PDF file.)
\end_deeper 
\layout Subsection

Mind-Body Problem
\layout Standard

This unit \SpecialChar \ldots{}

\layout Description

1/26 Bownds, Chapter 1: 
\begin_inset Quotes eld
\end_inset 

Thinking about Thinking
\begin_inset Quotes erd
\end_inset 


\layout Description

1/31 Descartes, Meditations 1 and 2 (.PDF file).
\layout Description

2/2 Cooper, 
\begin_inset Quotes eld
\end_inset 

The Emotional Life of the Wise,
\begin_inset Quotes erd
\end_inset 

 unpublished manuscript.
 (.PDF file.)
\layout Standard


\begin_inset ERT
status Collapsed

\layout Standard

\backslash 
framebox{
\backslash 
parbox[t][1
\backslash 
totalheight]{.99
\backslash 
textwidth}{
\end_inset 


\series bold 
Important Note:
\series default 
 The Philosophy Department is hosting a conference in Moral Pyschology on
 February 4--5.
 \SpecialChar \ldots{}

\begin_inset ERT
status Collapsed

\layout Standard
}}
\end_inset 


\layout Description

2/7 Nagel, 
\begin_inset Quotes eld
\end_inset 

What Is It Like to Be a Bat?
\begin_inset Quotes erd
\end_inset 

 
\emph on 
Philosophical Review
\emph default 
, 
\emph on 
83
\emph default 
 (1974), 435--50.
 
\begin_inset LatexCommand \url{http://links.jstor.org/sici?sici=0031-8108%28197410%2983%3A4%3C435%3AWIILTB%3E2.0.CO%3B2-Y}

\end_inset 

.
\layout Description

\SpecialChar \ldots{}

\the_end
