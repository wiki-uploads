Input theorem.inc

Style Theorem
  LabelString           "Theorem"
  Preamble
\newtheorem*{theorem}{Theorem}
  EndPreamble
End

Style Corollary
  LabelString           "Corollary"
  Preamble
\newtheorem*{corollary}{Corollary}
  EndPreamble
End

Style Lemma
  LabelString           "Lemma"
  Preamble
\newtheorem*{lemma}{Lemma}
  EndPreamble
End

Style Claim
  LabelString           "Claim"
  Preamble
\newtheorem*{claim}{Claim}
  EndPreamble
End
