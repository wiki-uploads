
# Set movie name
Style "Set movie name"
  LatexType         Command
  LatexName         setMovieName
  KeepEmpty         0
  Margin            Static
  ParIndent	    M
  NextNoIndent      1
  ParSkip           0.4
  TopSep            1.0
  BottomSep         1.0
  ParSep            0.5
  Align             Left
  AlignPossible     Left
  LabelType         Static
  LabelBottomSep    0.5
  LeftMargin        "Set movie name:00000"
  LabelString       "Set movie name   "

  Font 
    Family          Sans
    Series          Bold
    Size            Large
    Color	    Red
  EndFont
  LabelFont 
    Family          Sans
    Size            Normal
    Color	    Red
  EndFont

  Preamble
    \newcommand{\movieName}{}
    \newcommand{\setMovieName}[1]{\renewcommand{\movieName}[1]{#1}}
  EndPreamble
End

Style "Run movie"
  LatexType         Command
  LatexName         RunMovie
  KeepEmpty         0
  Margin            Static
  ParIndent	    M
  NextNoIndent      1
  ParSkip           0.4
  TopSep            1.0
  BottomSep         1.0
  ParSep            0.5
  Align             Left
  AlignPossible     Left
  LabelType         Static
  LabelBottomSep    0.5
  LeftMargin        "Play movie:00000"
  LabelString       "Run movie   "

  Font 
    Family          Sans
    Series          Bold
    Size            Large
    Color	    Red
  EndFont
  LabelFont 
    Family          Sans
    Size            Normal
    Color	    Red
  EndFont

  Preamble
    \newcommand{\RunMovie}[1]{\href{run:\movieName{}}{#1}}
  EndPreamble
End
