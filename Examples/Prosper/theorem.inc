Style Theorem
  Margin                First_Dynamic
  LatexType             Environment
  LatexName             theorem
  NextNoIndent          1
  LabelSep              xx
  ParIndent             MMM
  ParSkip               0.4
  ItemSep               0.2
  TopSep                0.7
  BottomSep             0.7
  ParSep                0.3
  Align                 Block
  AlignPossible         Block, Left
  LabelType             Static
  LabelString           "Theorem #."
  Font
    Shape               Italic
    Size                Normal
  EndFont
  LabelFont
    Shape               Up
    Series              Bold
  EndFont
End

Style Corollary
  CopyStyle             Theorem
  LatexName             corollary
  LabelString           "Corollary #."
  Preamble
\newtheorem{corollary}[theorem]{Corollary}
  EndPreamble
End

Style Lemma
  CopyStyle             Theorem
  LatexName             lemma
  LabelString           "Lemma #."
  Preamble
\newtheorem{lemma}[theorem]{Lemma}
  EndPreamble
End

Style Claim
  CopyStyle             Theorem
  LatexName             claim
  LabelString           "Claim #."
  Preamble
\newtheorem{claim}[theorem]{Claim}
  EndPreamble
End

Style Proof
  Margin                First_Dynamic
  LatexType             Environment
  LatexName             myproof
  NextNoIndent          1
  LabelSep              xx
  ParIndent             MMM
  ParSkip               0.4
  ItemSep               0.2
  TopSep                0.7
  BottomSep             0.7
  ParSep                0.3
  Align                 Block
  AlignPossible         Block, Left
  LabelType             Static
  LabelString           "Proof:"
  EndLabelType		Filled_Box
  Font
    Shape               Up
    Size                Normal
  EndFont
  LabelFont
    Shape               Italic
  EndFont
  Preamble
\newenvironment{myproof}{\begin{proof}}{\end{proof}\endtrivlist}
  EndPreamble
End

