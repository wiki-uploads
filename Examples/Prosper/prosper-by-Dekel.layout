#% Do not delete the line below; configure depends on this
#  \DeclareLaTeXClass[prosper]{slides (prosper by Dekel)}
#
# By:	Dekel Tsur <dekelts@tau.ac.il>
# Modified by:	Christian Ridderström <christian.ridderstrom@home.se>
# jw:	Based on the Article textclass "article.layout"
# Chr:	Added paragraph style 'institution'
# Chr:	Added paragrpah style 'email'
# Chr:	Formatted to facilitate side-by-side comparison with prosper-by-Dekel.layout
#
#
# Structure, use the command:
# 	grep '\(^Style\)\|\(^Input\)\|\(^NoStyle\)' prosper-by-Dekel.layout | sed "  s/^/#	/"
# Paragraph styles, no-styles and input-statements:
#	Style Standard
#	Style Slide

#	Input theorem-unnumbered.inc
#	Input stdtitle.inc
#	Input stdlists.inc
#	Input stdcounters.inc





#	Style Institution
#	Style E-mail
#	Input lyxmacros.inc
#	NoStyle Address
#	NoStyle "Right Address"
#	Style Subtitle
#	Style Comment

#

# General textclass parameters

ClassOptions


	Other "contemporain,pdf,accumulate,slideColor,colorBG"
End




MaxCounter		Counter_Section










####################################
#
#  Layout-specific preamble
#
#####################################


Preamble
  % Weiss defines theorem
  % Weiss defines proof

  \newcounter{slidetype}
  \setcounter{slidetype}{0}
  \newcommand{\lyxendslide}{%
     \ifthenelse{\value{slidetype}>0}{\end{slide}}{}%
     \setcounter{slidetype}{0}%
  }





  \AtEndDocument{\lyxendslide}

  \newcommand{\ov}[2]{\lyxendslide\overlays{#1}{#2\lyxendslide}}

  \usepackage{amsthm}

  \AtBeginDocument{\renewcommand{\proofname}{\upshape\emph{Proof}}}
  \def\@addpunct#1{}
  \def\th@plain{
    \thm@headfont{\theoremheadercolor}
    \thm@headpunct{\theorembodycolor} % This is a hack,
    \theorembodycolor% body font
  }
EndPreamble


############################
#
# Standard style definition
# Always comes first.
#
############################

Style Standard
  Margin            Static
  LatexType         Paragraph
  LatexName         dummy
  ParIndent         M
  ParSkip           0.4
  Align             Block
  AlignPossible     Block, Left, Right, Center
  LabelType         No_Label
End

##############################
#
# New Definitions for Slides
#
##############################

Style Slide
  LatexType             Command
  LatexName             lyxnewslide
  Margin                Dynamic
  NextNoIndent          1

  NeedProtect           1
  ParSkip               0.4
  TopSep                1.3
  BottomSep             0.7
  ParSep                0.7
  Align                 Center
  AlignPossible         Block, Center, Left
  LabelType             Counter_Section



  LabelSep              xxxx
  Font

    Series              Bold
    Size                Larger

  EndFont
  LabelFont

    Size		Footnotesize
    Color		Blue
  EndFont

  Preamble
  \newcommand{\lyxnewslide}[1]{
     \lyxendslide
     \setcounter{slidetype}{1}
     \begin{slide}{{#1}}
  }
  EndPreamble
End


Input theorem-unnumbered.inc

Input stdtitle.inc
Input stdlists.inc
Input stdcounters.inc



#
# In prosper-by-Weiss.layout, the paragraph styles
#	Overlay, Note, InvisibleText, VisibleText
# that are not defined here
#


Style Institution
  CopyStyle Author
  LatexName institution
End

Style E-mail
  CopyStyle Author
  LatexName email
  Font 
    Family          Roman
    Size            normal
  EndFont
End

Input lyxmacros.inc
NoStyle Address
NoStyle "Right Address"

Style Subtitle
  Margin		Static
  LatexType		Command
  InTitle		1
  LatexName		subtitle
  ParSkip		0.4
  ItemSep		0
  TopSep		0
  BottomSep		1
  ParSep		1
  Align			Center
  AlignPossible		Center
  LabelType		No_Label

  # standard font definition
  Font 
    Size		Large
  EndFont
End


Style Comment
  Margin                First_Dynamic
  NextNoIndent		0
  LeftMargin            MM
  RightMargin           MM
  LabelFont
     Color	magenta
  EndFont
End
