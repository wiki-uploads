#!/bin/sh

# wrapper to run tex2lyx of lyx-devel (lyx-1.4.0) 
# and retrieve a lyx version for lyx-current (1.3.5)
# J.P. Chretien <chretien@cert.fr>
# 
# initial version march 2005
# correction 19 may 2005: deals correctly w dependencies
# correction 30 may 2005: correct a flaw in dependency management
#                         due to the fact that included file suffix
#                         remains .tex if <basename>.lyx exists
#
# this script comes with no warranty, use at your own risk.
# WARNING: lyx2lyx backconversion needs printing of depending files
#          DO NOT USE until the messages are commited to cvs version
#          (tex2lyx should print the names of all created
#           or updated lyx files)

# where is tex2lyx
LYXDEVEL="/usr/local/LyX/"

# usage
usage() {
    test $# -ge 1 && SCRIPT=`basename "$1"`
    shift
    echo "Usage: $SCRIPT <LaTeX file basename>,"  >&2
    echo "e.g. $SCRIPT foo to convert foo.tex" >&2
    echo $* >&2
    exit 1
}

# one argument needed
test $# -eq 1 || usage "$0"


# file exists and is redable
test -r $1.tex || usage "$0" "$1.tex non readable"

# check fo existing lyx file
test -r $1.lyx && usage "$0" "$1.lyx exists, remove or rename"
# temporary compulsory -f option to cope with multipart documents

# warning
echo "tex2lyx.sh will clobber all depending .lyx files"
echo "(called by include, input,  or verbatiminput). Continue ? y/N"
read ok
(test -n $ok && test $ok = 'y') || {
echo "Aborting..."
exit 1
}

#translation
echo "Converting $1.tex and dependant files to lyx format 241..."
$LYXDEVEL/bin/tex2lyx -f $1.tex 1> $1.lyx 2>$1.log ||  {
echo "Exiting..." 
\rm -f $1.lyx
exit 1
}
# find out depending files
cat $1.log
grep 'Creating file' $1.log >$2.log
grep 'Overwriting existing file' $1.log >>$2.log
sed -e 's/^.* file //' $2.log > $1.log

#conversion
echo "Converting back to lyx format 221..."
mv  $1.lyx $1.lyx-1.4
python $LYXDEVEL/share/lyx/lyx2lyx/lyx2lyx -t 221 -o $1.lyx $1.lyx-1.4\
 && echo "$1.lyx available."
rm $1.lyx-1.4
liste=`cat $1.log`
for F in $liste 
   do
mv $F $F-1.4
python $LYXDEVEL/share/lyx/lyx2lyx/lyx2lyx -t 221 -o $F $F-1.4 \
&& echo "$F available."
rm -f $F-1.4
   done
echo "Conversion completed."
rm $1.log

exit 0




