#! /bin/sh

usage() {
    test $# -ge 1 && SCRIPT=`basename "$1"`
    shift
    echo "Usage: $SCRIPT <LyX source directory>" >&2
    echo $* >&2
    exit 1
}

# It appears that this environment variable is not defined on Sun,
# so just define it.
PWD=`pwd`

# document the created archive name
KERNEL=`uname -s`
echo "$KERNEL" | grep MINGW >/dev/null && KERNEL=win32
ARCH=`uname -p`

ARCHIVEBASE="tex2lyx_${KERNEL}_${ARCH}_"`date +%d%b%y`


test $# -eq 1 || usage "$0"
cd "$1" ||  usage "$0"
./autogen.sh || usage "$0" "autogen.sh failed"

BUILDDIR=build-tex2lyx
test -d "$BUILDDIR" || {
    mkdir "$BUILDDIR" || usage "$0" "mkdir $BUILDDIR failed"
}

cd "$BUILDDIR" || usage "$0" "cd $BUILDDIR failed"

INSTALLDIR=LyX-1.4
CONFIGURE="../configure --disable-debug --enable-optimization --prefix='$PWD/$INSTALLDIR' --with-frontend=foo"
echo $CONFIGURE
eval "${CONFIGURE}" || usage "$0" "configure failed"

(cd boost && make) || usage "$0" "make in boost failed"
(cd src/support && make) || usage "$0" "make in src/support failed"
(cd src/tex2lyx && make) || usage "$0" "make in src/tex2lyx failed"
(cd src/tex2lyx && make install) || usage "$0" "make install in src/tex2lyx failed"
(cd lib && make install) || usage "$0" "make install in lib failed"

# Remove stuff that's not needed by tex2lyx.

cd $PWD
RESOURCES="$INSTALLDIR"/`ls $INSTALLDIR | sed '/bin/d;/man/d'`
echo $RESOURCES
test -d "$RESOURCES" || \
    usage "$0" "Unable to ascertain installed resources dir $RESOURCES"
BINDIR="$INSTALLDIR"/bin

rm -f  "$RESOURCES"/charset.alias
rm -rf "$RESOURCES"/locale
rm -f  "$BINDIR"/noweb2lyx
# Gets tex2lyx and tex2lyx.exe
strip "$BINDIR"/tex2lyx*

(
  cd "$RESOURCES"/lyx
  rm -f configure encodings external_templates languages lyxrc.example symbols
# 02/11/05, J>P. Chr�tien, <chretien@cert.fr> 
# removed the scripts dir from the list as tex2lyx needs layout2layout
#  rm -rf bind clipart doc examples help images kbd scripts templates tex ui xfonts
  rm -rf bind clipart doc examples help images kbd templates tex ui xfonts
)


# build the bundle
case $KERNEL in
    win32)
	which zip || {
	    PATH="/c/Program Files/ZIP":$PATH
	    export PATH
	}

	which zip || {
	    echo "zip is not in your PATH. Please add it here." >&2
	    exit 1
	}

	ARCHIVE="$ARCHIVEBASE".zip
	rm -f "$ARCHIVE"
	zip "$ARCHIVEBASE" `find "$INSTALLDIR" -name '*'`
	;;
    *)
	# Not all tars understand "cvzf", so do this in two steps.
	ARCHIVE="$ARCHIVEBASE".tar
	rm -f "$ARCHIVE"
	tar cvf "$ARCHIVE" "$INSTALLDIR"
	rm -f "$ARCHIVE".gz
	gzip -9 "$ARCHIVE"
	ARCHIVE="$ARCHIVE".gz
	;;
esac

echo "$PWD/$ARCHIVE available"
rm -rf "$INSTALLDIR"
