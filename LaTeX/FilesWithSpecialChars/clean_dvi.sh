#! /bin/sh

# file clean_dvi.sh
# This file is part of LyX, the document processor.
# Licence details can be found in the file COPYING
# or at http://www.lyx.org/about/licence.php3

# author Angus Leeming
# Full author contact details are available in the file CREDITS
# or at http://www.lyx.org/about/credits.php

# Usage:
#     sh clean_dvi.sh infile.dvi outfile.dvi

# clean_dvi modifies the input .dvi file so that
# dvips and yap (a dvi viewer on Windows) can find
# any embedded PostScript files whose names are protected
# with "-quotes.

# It works by:
# 1 translating the machine readable .dvi file to human
#   readable .dtl form,
# 2 manipulating any references to external files
# 3 translating the .dtl file back to .dvi format.

# It requires dv2dt and dt2dv from the DTL dviware package
# http://www.ctan.org/tex-archive/dviware/dtl/


find_exe() {
    # find_exe expects a single argument
    test $# -eq 1 || return 1

    # Some flavours of `which` return 1 on failure.
    output=`which "$1" 2>/dev/null` || return 1

    # Some flavours of `which` (eg Tru64 Unix's) report a failure
    # to find the executable to STDOUT with the message
    # 'no <exe> in ...'
    echo $output | grep '^no ' >/dev/null && return 1

    # Looks like we found the thing
    return 0
}


error() {
    echo "$@" >&2
    exit 1
}


usage() {
    echo "Usage: `basename $0` in.dvi out.dvi"
}


# First establish whether the require executables exist
# and that the expected information has been input on the
# command line.
PATH=$HOME/dtl:$PATH

find_exe dv2dt || error "Please install 'dv2dt'"
find_exe dt2dv || error "Please install 'dv2dt'"
find_exe sed   || error "Please install 'sed'"


test $# -eq 2 || error `usage $0`

INFILE="$1"
OUTFILE="$2"

# The sed script used to manipulate the DTL file
SEDCOMMANDS='

# It transpires that "-quotes within the filename declaration
# confuse both dvips and yap (a dvi viewer on Windows).
# Entries in the .dtl file are of the form:
# ...PSfile=""a space".eps" llx=...
# Manipulate them to
# ...PSfile="a space.eps" llx=...

/PSfile=".*" llx/{

# Save the original in the hold space.
h

# Replace the file name with a placeholder "\n"
s/\(PSfile="\).*\(" llx=\)/\1\
\2/

# Exchange the contents of the hold and pattern space
x

# Remove everything except the filename
s/.*PSfile="//
s/" llx=.*//

# Remove any "-quotes in the filename
s/"//g

# Append the contents of the hold space to those of the pattern space
G

# Insert the modified file name (everything up to the first "\n")
# into the position of the placeholder (the second "\n").
s/\([^\n]*\)\n\([^\n]*\)\n/\2\1/
}'

# Manipulate the dvi file
dv2dt "$INFILE" | sed "$SEDCOMMANDS" | dt2dv -si "$OUTFILE" || exit 1

# The end
