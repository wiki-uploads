#!/bin/bash

# Setup global variables

#
# Function:	Check if an attachment file exists.
#		Returns with -1 if it exists.
# In:		Number
# Example:	attachmentFileExists 10 && echo Echoed when 10 exists
#		attachmentFileExists 11 || echo Echoed when 11 does not exist
function attachmentFileExists () {
    if [ -r a.$1 ]; then
	return 0
    fi
    if [ ! -r a.$1 ]; then
	return -1
    fi
}


#
# Function:	Check if an attempted download of an attachment actually
#		resulted in a wget for an attachment that does not exist.
#		If the attachment in reality is a HTML-file that says
#		that this attachment does not exist, then that downloaded
#		attachment is deleted.
# In:		$1 = number
# Examples:	attachmentIsNotInvalid 2000 && echo Attachment 2000 is not invalid
#		attachmentIsNotInvalid 10 || echo Attachment 10 does not exist or is invalid
function attachmentIsNotInvalid () {
    attachmentFileExists $1 || return -1
    cat a.$1 | grep -q "Attachment #$1 does not exist" && return -1
    return 0
}


# Function: Download a specific attachment from bugzilla
# Input: The number of the attachment
# Example:	downloadAttachment 57
function downloadAttachment () {
    attachmentFileExists $1 || \
	wget -q -O a.$1 "http://bugzilla.lyx.org/attachment.cgi?id=$1&action=view"
}

#
# Function:	Download a sequence of attachments
# Input: 	$1 = Start number
#		$2 = End number
# Example:	downloadAttachments 10 20
function downloadAttachments () {
    for number in `seq $1 $2`; do
	downloadAttachment $number
    done
}

#
# Function:	Download attachments until an invalid one is found.
#		Then delete that attachment and return.
# In:		Starting number
function downloadRemainingAttachments () {
    let n=$1
    downloadAttachment $n
    while attachmentIsNotInvalid $n; do
	let n=$n+1
	echo Acquiring attachment $n
	downloadAttachment $n
    done;
    attachmentIsNotInvalid $n || rm a.$n

}

#
# Function:	One test to see if the file is spam. This test checks
#		if the file is of a "spam type" according to 'file'.
#		It is considered spam if it is of one of the following
#		types:
#			'HTML document text'
#			'ASCII text, ...'
# In:		$1 = The number of the attachment
# 
function attachmentIsSpam () {
    file_type_maybe_spam=0
    fileResult=`file a.$1`
    echo $fileResult | grep -q ASCII && file_type_may_be_spam=1
    echo $fileResult | grep -q HTML && file_type_may_be_spam=1
    
    if [[ $file_type_may_be_spam -ne 0 ]]; then
	
	# Check if the file contains '<script'
	file_contains_script=0
	cat a.$1 | grep -iq '< *script' && file_contains_script=1

	if [[ $file_contains_script -ne 0 ]]; then
	    return 0;
	fi;
    fi;
    return -1;			# Not detected as spam
}

#
# Flow:
#	Download all attachments
#

let nMin=1830			# Min number to test, 1616 is the 1st spam
let nMax=2000			# Max number to test

# Download any new attachments, starting from number $1
echo Downloading attachments...
downloadRemainingAttachments $nMin

echo Analyzing downloaded attachments
echo 'Note: "Tagged" attachments are not re-analyzed'
let n=$nMin
while [ $n -le $nMax ]; do
    # Only process downloaded attachments
    if [ -r a.$n ]; then
        # Don't process attachments already tagged as spam
	if [ ! -r spam.$n ]; then
            # Don't process attachments already tagged as spam candidates
	    if [ ! -r spam.$n.candidate ]; then
		# Check for tag indicating it is _not_ spam
		if [ ! -r notspam.$n ]; then
		    if attachmentIsSpam $n; then
			echo $n is possible spam
			ln -s a.$n spam.$n.candidate
		    fi;
		fi;
	    fi;
	fi

    fi;
    let n=$n+1
done;

