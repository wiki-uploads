This is an uploaded file, it is just here as a demonstration.
It is placed in the directory

   uploads/Site/aboutUploading/

and called

    anUploadedFile.txt

The URI of this file is:

    http://wiki.lyx.org/uploads/Site/aboutUploading/anUploadedFile.txt

which is equivalent to:

    Uploads:Site/aboutUploading/anUploadedFile.txt

when using the prefix InterWiki-prefix 'Uploads'. Assuming that the
current page is in the group Site, the equivalent markup is:

    Attach:aboutUploading/anUploadedFile

Otherwise, an absolute path can be used as in the following example markup:

    Attach:/Site/aboutUploading/anUploadedFile.txt

/Christian
