# Author : David L. Johnson <dlj0@lehigh.edu>
# Probably broken by Jean-Marc Lasgouttes <Jean-Marc.Lasgouttes@inria.fr>
# modified and modularized by Emmanuel GUREGHIAN <gureghia@boston.bertin.fr>
# Paul Rubin <rubin@msu.edu>: added Assumption environment

# These are only the theorems styles environnements
# The environnements defined are :
# - Theorem
# - Corollary
# - Lemma
# - Proposition
# - Conjecture
# - Criterion
# - Algorithm
# - Fact
# - Axiom
# - Definition
# - Example
# - Condition
# - Problem
# - Exercise
# - Remark
# - Claim
# - Note
# - Notation
# - Summary
# - Acknowledgement
# - Case
# - Conclusion
# - Assumption


Format 2
Style Theorem
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{thm}{Theorem}
	EndPreamble
End


Style Corollary
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{cor}{Corollary}
	EndPreamble
End


Style Lemma
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{lem}{Lemma}
	EndPreamble
End


Style Proposition
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{prop}{Proposition}
	EndPreamble
End


Style Conjecture
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{conjecture}{Conjecture}
	EndPreamble
End


Style Criterion
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{criterion}{Criterion}
	EndPreamble
End


Style Algorithm
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{algorithm}{Algorithm}
	EndPreamble
End


Style Fact
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{fact}{Fact}
	EndPreamble
End


Style Axiom
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{ax}{Axiom}
	EndPreamble
End


Style Definition
	Preamble
	 \theoremstyle{definition}
	 \newtheorem{defn}{Definition}
	EndPreamble
End


Style Example
	Preamble
	 \theoremstyle{definition}
	  \newtheorem{example}{Example}
	EndPreamble
End


Style Condition
	Preamble
	 \theoremstyle{definition}
	  \newtheorem{condition}{Condition}
	EndPreamble
End


Style Problem
	Preamble
	 \theoremstyle{definition}
	  \newtheorem{problem}{Problem}
	EndPreamble
End


Style Exercise
	Preamble
	 \theoremstyle{definition}
	  \newtheorem{xca}{Exercise}
	EndPreamble
End


Style Remark
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{rem}{Remark}
	EndPreamble
End


Style Claim
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{claim}{Claim}
	EndPreamble
End


Style Note
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{note}{Note}
	EndPreamble
End


Style Notation
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{notation}{Notation}
	EndPreamble
End


Style Summary
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{summary}{Summary}
	EndPreamble
End


Style Acknowledgement
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{acknowledgement}{Acknowledgement}
	EndPreamble
End


Style Case
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{case}{Case}
	EndPreamble
End


Style Conclusion
	Preamble
	  \theoremstyle{remark}
	  \newtheorem{conclusion}{Conclusion}
	EndPreamble
End


Style Assumption
	Preamble
	  \theoremstyle{plain}
	  \newtheorem{assumption}{Assumption}
	EndPreamble
End
