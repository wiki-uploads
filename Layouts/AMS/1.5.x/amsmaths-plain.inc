# Author : David L. Johnson <dlj0@lehigh.edu>
# Probably broken by Jean-Marc Lasgouttes <Jean-Marc.Lasgouttes@inria.fr>
# modified and modularized by Emmanuel GUREGHIAN <gureghia@boston.bertin.fr>
# Tinkered with Sep. '07 by Paul Rubin <rubin@msu.edu>


# These are only the plain (unnumbered) theorem styles environments.
# Both starred and unstarred versions are included for compatibility
# with the other AMS classes.
# The environments defined are:
# - Theorem
# - Corollary
# - Lemma
# - Proposition
# - Conjecture
# - Criterion
# - Algorithm
# - Fact
# - Axiom
# - Definition
# - Example
# - Condition
# - Problem
# - Exercise
# - Remark
# - Claim
# - Note
# - Notation
# - Summary
# - Acknowledgement
# - Conclusion
# - Assumption
# - Case (regular only -- defined as an enumeration)


Format 4

Style Theorem
	LatexName             thm*
	LabelString           "Theorem."
End


Style Corollary
	LatexName             cor*
	LabelString           "Corollary."
End


Style Lemma
	LatexName             lem*
	LabelString           "Lemma."
End


Style Proposition
	LatexName             prop*
	LabelString           "Proposition."
End


Style Conjecture
	LatexName             conjecture*
	LabelString           "Conjecture."
End


Style Criterion
	LatexName             criterion*
	LabelString           "Criterion."
End


Style Algorithm
	LatexName             algorithm*
	LabelString           "Algorithm."
End


Style Fact
	LatexName             fact*
	LabelString           "Fact."
End


Style Axiom
	LatexName             ax*
	LabelString           "Axiom."
End


Style Definition
	LatexName             defn*
	LabelString           "Definition."
End


Style Example
	LatexName             example*
	LabelString           "Example."
End


Style Condition
	LatexName             condition*
	LabelString           "Condition."
End


Style Problem
	LatexName             problem*
	LabelString           "Problem."
End


Style Exercise
	LatexName             xca*
	LabelString           "Exercise."
End


Style Remark
	LatexName             rem*
	LabelString           "Remark."
End


Style Claim
	LatexName             claim*
	LabelString           "Claim."
End


Style Note
	LatexName             note*
	LabelString           "Note."
End


Style Notation
	LatexName             notation*
	LabelString           "Notation."
End


Style Summary
	LatexName             summary*
	LabelString           "Summary."
End


Style Acknowledgement
	LatexName             acknowledgement*
	LabelString           "Acknowledgement."
End


Style Conclusion
	LatexName             conclusion*
	LabelString           "Conclusion."
End


Style Assumption
	LatexName             assumption*
	LabelString           "Assumption."
End
