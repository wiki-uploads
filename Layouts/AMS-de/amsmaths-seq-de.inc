
Input amsmaths-seq.inc

Style Theorem
  Preamble
    \theoremstyle{plain}    
    \newtheorem{thm}{Theorem} 
  EndPreamble
End  

Style Corollary
  Preamble
    \theoremstyle{plain}    
    \newtheorem{cor}{Korollar} 
  EndPreamble
End  

Style Lemma
  Preamble
    \theoremstyle{plain}    
    \newtheorem{lem}{Lemma} 
  EndPreamble
End  

Style Proposition
  Preamble
    \theoremstyle{plain}    
    \newtheorem{prop}{Feststellung} 
  EndPreamble
End  

Style Conjecture
  Preamble
    \theoremstyle{plain}    
    \newtheorem{conjecture}{Vermutung} 
  EndPreamble
End  

Style Criterion
  Preamble
    \theoremstyle{plain}    
    \newtheorem{criterion}{Kriterium} 
  EndPreamble
End  

Style Algorithm
  Preamble
    \theoremstyle{plain}    
    \newtheorem{algorithm}{Algorithmus} 
  EndPreamble
End  

Style Fact
  Preamble
    \theoremstyle{plain}    
    \newtheorem{fact}{Tatsache}
  EndPreamble
End  

Style Axiom
  Preamble
    \theoremstyle{plain}    
    \newtheorem{ax}{Axiom}
  EndPreamble
End  

Style Definition
  Preamble
   \theoremstyle{definition}
   \newtheorem{defn}{Definition}
  EndPreamble
End  

Style Example
  Preamble
   \theoremstyle{definition}
    \newtheorem{example}{Beispiel}
  EndPreamble
End  

Style Condition
  Preamble
   \theoremstyle{definition}
    \newtheorem{condition}{Bedingung}
  EndPreamble
End  

Style Problem
  Preamble
   \theoremstyle{definition}
    \newtheorem{problem}{Problem}
  EndPreamble
End  

Style Exercise
  Preamble
   \theoremstyle{definition}
    \newtheorem{xca}{\"Ubung}
  EndPreamble
End  

Style Remark
  Preamble
    \theoremstyle{remark}
    \newtheorem{rem}{Bemerkung}
  EndPreamble
End  

Style Claim
  Preamble
    \theoremstyle{remark}    
    \newtheorem{claim}{Behauptung}
  EndPreamble
End  

Style Note
  Preamble
    \theoremstyle{remark}    
    \newtheorem{note}{Notiz}
  EndPreamble
End  

Style Notation
  Preamble
    \theoremstyle{remark}    
    \newtheorem{notation}{Notation}
  EndPreamble
End  

Style Summary
  Preamble
    \theoremstyle{remark}    
    \newtheorem{summary}{Zusammenfassung}
  EndPreamble
End  

Style Acknowledgement
  Preamble
    \theoremstyle{remark}    
    \newtheorem{acknowledgement}{Danksagung}
  EndPreamble
End  

Style Case
  Preamble
    \theoremstyle{remark}    
    \newtheorem{case}{Fall}
  EndPreamble
End  

Style Conclusion
  Preamble
    \theoremstyle{remark}    
    \newtheorem{conclusion}{Schlussfolgerung}
  EndPreamble
End  

