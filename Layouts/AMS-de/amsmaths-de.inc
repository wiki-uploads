
Input amsmaths.inc

Style Theorem*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{thm*}{Theorem} 
  EndPreamble
End  

Style Corollary
  Preamble
    \theoremstyle{plain}    
    \newtheorem{cor}[thm]{Korollar} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Corollary*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{cor*}{Korollar}
  EndPreamble
End  

Style Lemma
  Preamble
    \theoremstyle{plain}    
    \newtheorem{lem}[thm]{Lemma} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Lemma*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{lem*}{Lemma} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Proposition
  Preamble
    \theoremstyle{plain}    
    \newtheorem{prop}[thm]{Feststellung} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Proposition*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{prop*}{Feststellung} 
  EndPreamble
End  

Style Conjecture
  Preamble
    \theoremstyle{plain}    
    \newtheorem{conjecture}[thm]{Vermutung} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Conjecture*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{conjecture*}{Vermutung} 
  EndPreamble
End 

Style Criterion
  Preamble
    \theoremstyle{plain}    
    \newtheorem{criterion}[thm]{Kriterium} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Algorithm
  Preamble
    \theoremstyle{plain}    
    \newtheorem{algorithm}[thm]{Algorithmus} %%Delete [thm] to re-start numbering
  EndPreamble
End  

Style Fact
  Preamble
    \theoremstyle{plain}    
    \newtheorem{fact}[thm]{Tatsache} 
  EndPreamble
End  

Style Fact*
  Preamble
    \theoremstyle{plain}    
    \newtheorem*{fact*}{Tatsache} 
  EndPreamble
End  

Style Axiom
  Preamble
    \theoremstyle{plain}    
    \newtheorem{ax}[thm]{Axiom} 
  EndPreamble
End  

Style Definition
  Preamble
   \theoremstyle{definition}
   \newtheorem{defn}[thm]{Definition}
  EndPreamble
End  

Style Definition*
  Preamble
   \theoremstyle{definition}
   \newtheorem*{defn*}{Definition}
  EndPreamble
End  

Style Example
  Preamble
   \theoremstyle{definition}
    \newtheorem{example}[thm]{Beispiel}
  EndPreamble
End  

Style Example*
  Preamble
   \theoremstyle{definition}
    \newtheorem*{example*}{Beispiel}
  EndPreamble
End  

Style Condition
  Preamble
   \theoremstyle{definition}
    \newtheorem{condition}[thm]{Bedingung}
  EndPreamble
End  

Style Condition*
  CopyStyle		Definition*
  LatexName		condition*
  LabelString		"Condition."
  Preamble
   \theoremstyle{definition}
    \newtheorem*{condition*}{Bedingung}
  EndPreamble
End  

Style Problem
  Preamble
   \theoremstyle{definition}
    \newtheorem{problem}[thm]{Problem}
  EndPreamble
End  

Style Problem*
  CopyStyle		Definition*
  LatexName		problem*
  LabelString		"Problem."
  Preamble
   \theoremstyle{definition}
    \newtheorem*{problem*}{Problem}
  EndPreamble
End  

Style Exercise
  Preamble
   \theoremstyle{definition}
    \newtheorem{xca}[section]{\"Ubung}%%Delete [section] for sequential numbering
  EndPreamble
End  

Style Exercise*
  CopyStyle		Definition*
  LatexName		xca*
  LabelString		"Exercise."
  Preamble
   \theoremstyle{definition}
    \newtheorem*{xca*}{\"Ubung}
  EndPreamble
End  

Style Remark
  Preamble
    \theoremstyle{remark}
    \newtheorem{rem}[thm]{Bemerkung}
  EndPreamble
End  

Style Remark*
  Preamble
    \theoremstyle{remark}
    \newtheorem*{rem*}{Bemerkung}
  EndPreamble
End  

Style Claim
  Preamble
    \theoremstyle{remark}    
    \newtheorem{claim}[thm]{Behauptung}
  EndPreamble
End  

Style Claim*
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{claim*}{Behauptung}
  EndPreamble
End  

Style Note
  Preamble
    \theoremstyle{remark}    
    \newtheorem{note}[thm]{Notiz} 
  EndPreamble
End  

Style Note*
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{note*}{Notiz} 
  EndPreamble
End  

Style Notation
  Preamble
    \theoremstyle{remark}    
    \newtheorem{notation}[thm]{Notation} 
  EndPreamble
End  

Style Notation*
  CopyStyle		Remark*
  LatexName		notation*
  LabelString		"Notation."
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{notation*}{Notation} 
  EndPreamble
End  

Style Summary
  Preamble
    \theoremstyle{remark}    
    \newtheorem{summary}[thm]{Zusammenfassung} 
  EndPreamble
End  

Style Summary*
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{summary*}{Zusammenfassung}
  EndPreamble
End  

Style Acknowledgement
  Preamble
    \theoremstyle{remark}    
    \newtheorem{acknowledgement}[thm]{Danksagung} 
  EndPreamble
End  

Style Acknowledgement*
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{acknowledgement*}{Danksagung} 
  EndPreamble
End  

Style Case
  Preamble
    \theoremstyle{remark}    
    \newtheorem{case}{Fall} %%Numbering of Cases not keyed to sections 
  EndPreamble
End  

Style Conclusion
  Preamble
    \theoremstyle{remark}    
    \newtheorem{conclusion}[thm]{Schlussfolgerung} 
  EndPreamble
End  

Style Conclusion*
  Preamble
    \theoremstyle{remark}    
    \newtheorem*{conclusion*}{Schlussfolgerung} 
  EndPreamble
End  

