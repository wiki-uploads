test $# -eq 1 || exit 1
test -d "$1" || exit 1

'ls' -l "$1" | \
sed 's/[	 ]\{1,\}/ /g' | \
cut -d' ' -f'5,9' | \
sed '/^ *$/d
s/[0-9]\{3\} / /
s@\([0-9]*\) *\([^ ]*\)@[[http://wiki.lyx.org/uploads/Windows/Aspell6/\2 |\2]] (\1kB)@' | \
sed '$!{
N
s/\n/ || /
}
s/^/|| /
s/$/ ||/
'