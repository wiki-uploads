#!/bin/sh
#
# Wrapper skeleton for invoking a native command named "command_name"
#
#!/bin/sh
PARAMS=""
for i in "$@"; do
  case "$i" in
    -*=/*) i="`echo "$i" | \
               sed 'h;s,^[^=]*=\(/.*\)$,cygpath -w -- "\1",e;x;\
                    s,=.*$,=,;G;s,\n,,'`" ;;
    -*) ;;
    */*) i="`cygpath -w -- "$i"`" ;;
    *) ;;
  esac
  PARAMS="$PARAMS '$i'"
done
eval "set -- $PARAMS"
command_name "$@"
