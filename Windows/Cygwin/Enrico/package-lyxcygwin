#!/bin/bash

# This script installs lyxrc.dist and sets correct permissions for the fonts.

Error ()
{
    test $# -gt 0 && {
	echo $* >&2
	echo >&2
    }

    echo "Expecting \"`basename $0` lyxsysdir\"" >&2
    echo "where lyxsysdir is the LyX system directory" >&2
    exit 1
}

cygwin_packaging()
{
    # Add screen fonts and path prefix entries to lyxrc.dist
    LYXRCDIST="${LYX_SYSDIR}"/lyxrc.dist
    echo "\\screen_dpi 96" > ${LYXRCDIST}
    echo "\\screen_zoom 150" >> ${LYXRCDIST}
    echo "\\screen_font_roman \"Times New Roman\"" >> ${LYXRCDIST}
    echo "\\screen_font_sans \"Arial\"" >> ${LYXRCDIST}
    echo "\\screen_font_typewriter \"Courier New\"" >> ${LYXRCDIST}
    echo "\\preview_scale_factor 1.0" >> ${LYXRCDIST}
    echo "\\cursor_width 2" >> ${LYXRCDIST}
    echo "\\path_prefix \"/usr/local/bin:/usr/bin:/usr/X11R6/bin\"" >> ${LYXRCDIST}

    # Set correct permissions for the Bakoma fonts. For some reason, the
    # fonts must have the execute bit set, otherwise they are not found.
    chmod a+rx "${LYX_SYSDIR}"/fonts/*.ttf
}

test $# -eq 1 || Error

LYX_SYSDIR=$1
test -d "$LYX_SYSDIR" || Error "Directory '$LYX_SYSDIR' does not exist."
test -f "$LYX_SYSDIR/chkconfig.ltx" || \
    Error "Directory '$LYX_SYSDIR' does not appear to be the system LyX dir."

cygwin_packaging

# The end
