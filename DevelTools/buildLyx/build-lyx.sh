#!/bin/bash

S0=`basename $0`
D0=`dirname $0`

if [[ "x$1" == "x" || "x$1" == "x--help" || "x$1" == "x-h" ]]; then
    cat <<EOF
Syntax: $S0 [options]
or:	$S0 [options] init

The first form typically performs tasks to build a version of lyx from scratch.

The second form is used to create a development directory (\$LYXDEVEL)
and check out the CVS module 'lyx-devel' from the CVS server.

Options:
  -v			Be more verbose
  -n			Only print commands, do not actually run
  -f config-file	Name of configuration file to use
  -l log-file		Set name of log-file
  --no-rm		Don't remove previous build directory
  --no-cvs		Don't run cvs update and autogen.sh
  --no-configure	Don't run configure
  --no-make		Don't run make
  -h, --help		Show help
  --version		Report version

Example:
 # Read commands from xforms-devel.cfg
 $ $0 -f etc/xforms-devel.cfg

Note that the configuration file is first looked for in the current
directory, and then relative to where the script is located.

For further information, see 
	http://wiki.lyx.org/pmwiki.php/DevelScripts/BuildLyx

EOF
exit 0;
fi

if [[ "x$1" == "x--version" ]]; then printf "$S0 1.0\n"; exit 0; fi

# Use: Verbose 1 "Only printed if Verbose >= 1, arg: %s" "an argument"
# The first numeric argument can be skipped if the condition is onl that
# the verbosity > 0
function Verbose() {
    if [[ ${#1} -eq 1 && $Verbosity -lt $1 ]]; then return; fi;
    if [[ ${#1} -ne 1 && $Verbosity -eq 0 ]]; then return; fi;
    if [[ ${#1} -eq 1 ]]; then shift; fi;
    FMT="$1\n"
    shift;
    printf "$FMT" "$@"
}

function Log() {
    Verbose 0 "# ""$@" | tee --append $LogFile
}

# Use: Error -1 "This argument is wrong: %s" "an argument"
function Error() {
    ERROR_CODE=$1;
    shift;
    printf "%s: Error(%d) " "`basename $0`" $ERROR_CODE
    FMT="$1\n"
    shift
    printf "$FMT" "$@"
    exit $ERROR_CODE
}

# For some reason, 'cd' doesn't work when |tee is used...
function Do() {
    if (($DO)); then msg=""; else msg="## "; fi;
    if (($Verbosity >= 1)); then Log "%s%s" "$msg" "$*"; fi;
    fail=0
    if (($DO)); then
	if [[ "$1" == "cd" || "$1" == "pushd" || "$1" == "popd" ]]; then
	    printf "%s\n" "$*" >> $LogFile
	    printf "%s\n" "$*"
	    "$@"
	else
	    "$@" | tee --append $LogFile ;
	    fail=${PIPESTATUS[0]}
	fi;
    fi;
    return $fail
}

ConfigFilesRead=0
function ReadConfiguration() {
    Verbose 2 "Looking for configuration file"
    for FIL in "$@" ; do
	Verbose 3 "Checking for %s" "$FIL"
	if [[ -r $FIL ]]; then
	    Verbose 1 "Reading %s" "$FIL"
	    source $FIL
	    let ++ConfigFilesRead
	elif [[ -r $D0/$FIL ]]; then
	    Verbose 1 "Reading %s/%s" "$D0" "$FIL"
	    source $D0/$FIL
	    let ++ConfigFilesRead
	else
	    Verbose 2 "Configuration file %s not found" "$FIL"
	fi
    done
    if (( $ConfigFilesRead > 0 )); then
	Verbose 2 "Configuration after reading configuration files:"
	if (( $Verbosity >= 2 )); then ShowConfiguration; fi
    fi
}

#
# Login to CVS server or checkout a cvs module
# Syntax: CVS_Command login
# or:	  CVS_Command checkout <DestDir>
# where <DestDir> is the name of the destination directory
# (relative to $LYXDEVEL which is created if necessary).
# The release tag that's checked out is taken from $LYXTAG
# Example: CVS_Command checkout $LYXTAG
# will checkout to a directory with the same name as the tag
#
function CVS_Command() {
    if [[ "$1" == "login" ]]; then
	Verbose 0 "The anonymous password is 'lyx'"
	Do cvs -d $LYX_CVS_SERVER login
    elif [[ "$1" == "checkout" ]]; then
	Do mkdir -p $LYXDEVEL;
	Do pushd $LYXDEVEL
	Verbose 0 "\n\nGo have coffee or something..."
	Do cvs -d $LYX_CVS_SERVER co -d $2 -r $LYXTAG $LYX_CVS_MODULE
	Do popd
    fi;
}

#
# Set default values of variables
#
# Standard variables
Verbosity=0			# Controls the level of verbosity
DO=1				# Set to zero if nothing should be done
LogFile=`pwd`/build-lyx.log
#
# Script specific variables
LYX_CVS_SERVER=":pserver:anoncvs@anoncvs.us.lyx.org:/cvs/lyx"
LYX_CVS_MODULE="lyx-devel"
MODULE_LYX=lyx/devel/latest-xforms
MODULE_GCC=gcc/3.3.2
BuildPostfix=""
OPTS_CONFIGURE=()
DO_RM=1;			# Remove previous build directory?
DO_CVS=1;			# Do CVS update
DO_CONFIGURE=1;			# Run configure
DO_MAKE=1;			# Run make
USE_MODULES=0;			# Use modules?

function ShowConfiguration(){
    if (( $Verbosity >= 2 )); then
	Verbose "Variables in the script:"
	Verbose "DO=%s" "$DO"
	Verbose "buildDir=%s" "$buildDir"
	Verbose "BuildPostfix=%s" "$BuildPostfix"
	Verbose "LYXDEVEL=%s" "$LYXDEVEL"
	Verbose "LYXTAG=%s, LYXFRONTEND=%s" "$LYXTAG" "$LYXFRONTEND"
    fi;
}

# Initialize modules
if (($USE_MODULES)); then
    . /usr/local/lib/module/default/init/bash
    module rm lyx gcc		# Remove any lyx and gcc modules
fi;

# Parse arguments
savedArgs="$*"
for ARG in "$@"; do if [[ "$ARG" == "-v" ]]; then let ++Verbosity; fi; done;

until [[ $# -eq 0 ]]; do
    case "$1" in
	-v)     ;;		# Already parsed for
	-n)	DO=0 ;;
	--no-rm)	DO_RM=0 ;;
	--no-cvs)	DO_CVS=0 ;;
	--no-make)	DO_MAKE=0 ;;
	--no-configure)	DO_CONFIGURE=0 ;;
	-f)	ReadConfiguration $2; shift ;;
	-l)	LogFile=$2; shift ;;
	-*)	Error -1 "Unknown option: %s" "$1" ;;
	init)	break ;;
	*)      Error -2 "Unkown argument: %s" "$1" ;;
    esac
    shift;
done

if (( $ConfigFilesRead == 0 )); then
    Error -2 "No configuration file specified!"
fi;

# Initialize log-file
Verbose 0 "Log-file=%s\n" "$LogFile"

printf "# --------------------------------\n"\
"# Log file from executing:\n#  %s$ %s %s\n" \
"`pwd`" "$0""$savedArgs" > $LogFile
Log "Date: %s\n" "`date`"

# Verify validity of arguments
# if [[ "x$Adr" == "x" ]]; then Error "No address given!"; fi;

#
# -----------------------
# Actually do stuff...
#

# Handle initialization
if [[ "$1" == "init" ]]; then
    CVS_Command login
    CVS_Command checkout $LYXTAG
    exit 0;
fi;

# Add modules
if (($USE_MODULES)); then
    module add $MODULE_LYX $MODULE_GCC  2>&1 1>>$LogFile
    if (($?)); then Error -1 "While loading modules"; fi;
fi;

# Create buildDir if necessary
if [[ "$buildDir" == "" ]]; then
    buildDir=$LYXDEVEL/$LYXTAG/build-$LYXFRONTEND
    if [[ "x$BuildPostfix" != "x" ]]; then
	buildDir="$buildDir-$BuildPostfix";
    fi;
fi;
Log "Build directory=%s" "$buildDir"


GCC_VERSION="`gcc --version`"
Log "Using %s" "${GCC_VERSION%%Copyright*}"

if (($DO_RM)); then
    Log "Erasing and recreating build directory: %s" "$buildDir"
    Do rm -rf $buildDir && Do mkdir -p $buildDir
    if (($?)); then Error -2 "While erasing/creating build directory"; fi
else
    Log "Skipping erasing build directory"
fi;

if (($DO_RM || $DO_CVS)); then
    Log "Updating source and then running autogen.sh"
    cd $LYXDEVEL/$LYXTAG && Do cvs -q update -d && Do ./autogen.sh
    if (($?)); then Error -3 "While cvs update or autogen.sh"; fi
else
    Log "Skipping 'cvs update' and autogen"
fi;

if (($DO_RM || $DO_CVS || $DO_CONFIGURE)); then
    Log "Running configure"
    if (($DO)); then cd $buildDir; fi
    Do ../configure --prefix=$LYXPREFIX ${OPTS_CONFIGURE[*]}
    if (($?)); then Error -4 "While running configure"; fi
else
    Log "Skipping configure"
fi;

if (($DO_MAKE)); then
    Log "Running make"
    Do make -j 5
    if (($?)); then Error -5 "While running make"; fi
else
    Log "Skipping make"
fi;





