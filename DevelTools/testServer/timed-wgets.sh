#!/usr/bin/env bash
#Usage:
#  timed-wgets.sh <count> URI
#
LIMIT=$1
URI=$2

for ((a=1; a <= $LIMIT; a++))
do
 { { time wget -q -O /tmp/asda $URI && rm /tmp/asda ; } 2>&1 ; } ;
done