#!/usr/bin/env bash
#Usage:
#  timed-du.sh <count> URI <threshold_ms>
#
LIMIT=$1
URI=$2
TH_ms=$3
TIMEFORMAT='%3R'		#  Set three decimal places,  important

count=0
count_less=0
count_above=0

for ((a=1; a <= $LIMIT; a++))
do
  let count=$count+1
  t=`{ { time du $URI > /dev/null ; } 2>&1 ; }`
  t_ms=`echo $t | sed -e "  s/\.//"`		#  "multiply" by 1000
  let t_ms=10#$t_ms
  if (( "$t_ms" > "$TH_ms" ))
      then
      let count_above=$count_above+1
      printf "Overrun %3d/%-6d of 'du' (%d ms > %d ms)\n" $count_above $count $t_ms "$TH_ms"
#      echo "Overrun $count_above/$count of 'du' ($t_ms ms > $TH_ms ms)."
  else
      let count_below=$count_below+1
  fi 
done

echo "# overruns = $count_above / $count"
