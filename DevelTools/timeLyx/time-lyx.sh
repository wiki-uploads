#!/bin/bash

S0=`basename $0`

if [[ "X$1" == "X" || "X$1" == "X--help" || "X$1" == "X-h" ]]; then
    cat <<EOF
Syntax: $S0 [options] command-sequence
Example:
 $ $S0 "lyx-quit;" > result.txt

 $ $S0 "file-open UserGuide.lyx; lyx-quit;" > result.txt
(note you have to be in the same directory as UserGuide.lyx)

 $ $S0 -I ~/.lyx/doc "file-open DOC/UserGuide.lyx; lyx-quit;"

Options:
  -v			Be more verbose
  -n			Only print commands, do not actually run lyx
  -l lyx		Name of lyx binary
  -m descipriont	A description string
  -I ~/.lyx/doc		Defines replacement for string 'DOC'
  -f- 			Clear list of configuration files to look for
  -f config-file	Name of additional configuration files to be
			sourced. The default is to look for time-lyx.conf
			 in `dirname $0`/ and ./

EOF
exit
fi

#
# Time the execution of a of command (arg 1) with parameters in
# remaining arguments (four is max at the moment)
function timeCommand() {
    case $# in
	0)  printf "timeCommand: No arguments given!\n" ;;
	1)  $TIME $TIMEF "$TFMT" "$1" ;;
	2)  $TIME $TIMEF "$TFMT" "$1" "$2" ;;
	3)  $TIME $TIMEF "$TFMT" "$1" "$2" "$3" ;;
	4)  $TIME $TIMEF "$TFMT" "$1" "$2" "$3" "$4" ;;
	5)  $TIME $TIMEF "$TFMT" "$1" "$2" "$3" "$4" "$5" ;;
	*) printf "More arguments than supported!\n"; exit -1;
    esac
}

# Launch lyx and have it execute command sequence give by $*
# First argument is a description, and second is a command sequence
#
function LyxTest() {
    if [[ "$1" != "" ]]; then printf "Starting: %s\n" "$1"; fi
	
    if [[ $DO -ne 0 ]]; then
	Verbose "$ %s -x 'command-sequence %s'" "lyx" "$2"
	timeCommand $LYX -x "command-sequence $2"
	
	if [[ "$1" != "" ]]; then printf "Stopped: %s\n" "$1"; cat $OUT; fi
    else
	Verbose 0 "$ %s -x 'command-sequence %s'" "lyx" "$2"
    fi
}

# Used for debugging this script, it shows the arguments
function printArgs() {
    for (( ii=1; $ii <= $#; ++ii )) ; do
	printf "Arg %d: '%s'\n" "$ii" "${!ii}"
    done
}

# Use: Verbose 1 "Only printed if Verbose >= 1, arg: %s" "an argument"
# The first numeric argument can be skipped if the condition is onl that
# the verbosity > 0
function Verbose() {
    if [[ ${#1} -eq 1 && $Verbosity -lt $1 ]]; then return; fi;
    if [[ ${#1} -ne 1 && $Verbosity -eq 0 ]]; then return; fi;
    if [[ ${#1} -eq 1 ]]; then shift; fi;
    case $# in
	0) printf "Verbose without arguments\n" ;;
	1) printf "$1\n" ;;
	2) printf "$1\n" "$2" ;;
	3) printf "$1\n" "$2" "$3" ;;
	4) printf "$1\n" "$2" "$3" "$4" ;;
	5) printf "$1\n" "$2" "$3" "$4" "$5" ;;
	6) printf "$1\n" "$2" "$3" "$4" "$5" "$6" ;;
	*) printf "Too many arguments to Verbose!\n" ;;
    esac
}

# Use: Error -1 "This argument is wrong: %s" "an argument"
function Error() {
    ERROR_CODE=$1;
    shift;
    printf "%s: Error(%d) " "`basename $0`" $ERROR_CODE
    case $# in
	0) printf "called without arguments\n" ;;
	1) printf "$1\n" ;;
	2) printf "$1\n" "$2" ;;
	3) printf "$1\n" "$2" "$3" ;;
	4) printf "$1\n" "$2" "$3" "$4" ;;
	5) printf "$1\n" "$2" "$3" "$4" "$5" ;;
	6) printf "$1\n" "$2" "$3" "$4" "$5" "$6" ;;
	*) printf "Too many arguments to Verbose!\n" ;;
    esac
    exit $ERROR_CODE
}

function ShowConfiguration(){
    printf "LYX = %s\n" "$LYX"
    printf "CONFIG_FILE = %s\n" "${CONFIG_FILE[*]}"
    printf "DO = %s\n" "$DO"
    printf "Description = %s\n" "$Description"
    printf "CmdSeq      = %s\n" "'$CmdSeq'"
    printf "DOC = '%s'\n" "$DOC"
}

#
# Set default values of variables
#
OUT=/tmp/$$-timing.txt
LYX=lyx
TIME=/usr/bin/time 	# Must be GNU time
TFMT="real %e \tuser %U \tsys %S"
TIMEF="--output=$OUT -f"
DO=1
testCount=0
CONFIG_FILE=(`dirname $0`/time-lyx.conf ./time-lyx.conf)
Verbosity=0
DescriptionSet=0
UseDOC=0

until [[ $# -eq 0 ]]; do
    case "$1" in
	-l)	LYX=$2; shift ;;
	-f-)	CONFIG_FILE=() ;;
	-f)	CONFIG_FILE=(${CONFIG_FILE[*]} $2); shift ;;
	-v)	let ++Verbosity ;;
	-n)	DO=0 ;;
	-m)	Description="$2"; DescriptionSet=1; shift ;;
	-I)	DOC=$2; UseDOC=1; shift ;;
	-*)	Error -1 "Unknown option: %s" "$1" ;;
	*)
	    if [[ CmdSet -eq 0 ]]; then
		CmdSeq="$1"; shift;
		CmdSet=1
	    else
		Error -2 "Only one command sequence is allowed!"
	    fi ;;		
    esac
    shift;
done
Verbose "Output is verbose (verbosity = %d)" "$Verbosity"
Verbose 3 "Default configuration:"
if [[ $Verbosity -ge 3 ]]; then ShowConfiguration; fi

Verbose 2 "Looking for configuration files"
CONFIG_FILES_FOUND=0
for FIL in ${CONFIG_FILE[*]} ; do
    Verbose 3 "Checking for %s" "$FIL"
    if [[ -r $FIL ]]; then
	Verbose 2 "Reading %s" "$FIL"
	source $FIL
	let ++CONFIG_FILES_FOUND
    else
	Verbose 2 "Configuration file %s not found" "$FIL"
    fi
done

if [[ $CONFIG_FILES_FOUND -gt 0 ]]; then
    Verbose 2 "Configuration:"
    if [[ $Verbosity -ge 2 ]]; then ShowConfiguration; fi
fi

Verbose "LyX binary: %s" "$LYX"
if [[ ${#CmdSeq} -eq 0 ]]; then Error 1 "No command sequence given!"; fi
if [[ $UseDOC -ne 0 ]]; then
    Verbose 2 "Replacing DOC with '%s'" "$DOC"
    CmdSeq="${CmdSeq//DOC/$DOC}"
fi

LyxTest "$Description" "$CmdSeq"


