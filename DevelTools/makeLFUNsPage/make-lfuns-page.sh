#!/usr/local/bin/bash
#!/bin/bash	# Common location for bash program
#
# Homepage: http://wiki.lyx.org/pmwiki.php/DevelTools/MakeLFUNsPage
# Christian Riddertröm
# $Id: template.sh,v 1.1 2003/11/16 16:09:51 chr Exp $
#
S0=`basename $0`
if [[ "$1" == "" || "$1" == "-h" || "$1" == "--help" || "$1" == "-help" ]]; then
    cat <<EOF
Fcn:	Extract list of lfuns from a .C-file and optionall help from a .ui-file
Syntax:	$S0 [options]
Options:
	-h, --help, -help Show this help text
	-q	Be more quiet
	-v	Be more verbose
	-d	Debug script
	-n	Don't actually do anything
	-q	Be quiet
	-v	Be more verbose (increases verbosity)
	+v	Be less verbose (decreases verbosity)
	-n	No actions actually performed
	+n	Do actions (inverse of '-n'), typically unneccessary
	-d	Activate debugging of this script
	--test	Run self-test
	--version Show version n:o of this script
	-r rev	Use wget to retrieve release 'rev' from CVS web-interface
	--ui	Extract information from .ui-file
	--normal	Extract 'normal' functions
	--no-normal	Skip extracting 'normal' functions
	--only-normal	Only extract 'normal' functions
	--fonts, --no-fonts and --only-fonts  are similar to the above.
	--accents, --no-accents and --only-accents are similar to the above.
	--keep-tmp	Keep temporary files
	--wiki-output	Produce output in PmWiki-format
	--wiki-page	Produce a complete wiki-page (-r is required, and
			no other options work)

Examples:
# Extract 'normal' lfuns from release 1.3.3 (downloads file from the web)
	$S0 -r lyx-1_3_3
# Same thing, but using the latest (VCS) version
	$S0 -r HEAD

Note: The output is normally the string for the user-command, followed
by a TAB and possibly a help string (for release lyx-1_3_3). However, if
the option '--wiki-output' is given, then a resulting line looks like this:
||<user-command><TABs>||<help-text><TABs>||

To create a wiki page, use this sequence:
	$S0 -r lyx-1_3_3 --wiki-page > LyxFunctionList1-3-3.pmwiki
or this sequence (depending on if data should be extracted form stdtoolbars.ui)
	$S0 -r HEAD --ui --wiki-page > LyxFunctionListCVS.pmwiki
EOF
    exit 0
fi

CVS_Revision='$Revision: 1.1 $'
CVS_Name='Name: $'
Version=${CVS_Revision/\$Revision://}
[[ "$1" == --version ]] && { printf "%s\n" "$Version"; exit 0; }

# Syntax: Error [errNo] [formatString [arguments]]
function Error(){
    err=1; (("$1")) && err=$1;	# Force evaluation of error argument
    if (($#0)); then		# Any arguments?
	(("$1")) && shift	# If 1st argument is a number, then shift args
	(($#)) && msg=("$1" "${@:2}"); # If msg.args. left, then set $msg
    fi;
    printf "Error %d in %s: ${msg[0]}\n" $err "`basename $0`" "${msg[@]:1}" >&2
    exit $err
}

# Syntax: Verbose [verbosityLevel] formatString [arguments]
Verbosity=0			# Default is Verbosity=0, i.e. no messages
function Verbose() {
    if ((${#1} == 1)); then vLevel=$1; shift; else vLevel=1; fi
    if (($Verbosity >= $vLevel)); then printf "$1\n" "${@:2}"; fi;
}

# Syntax: Log formatString [arguments]
function Log() {
    printf "$1\n" "${@:2}"
}

#
# Set default values for variables
#
DO=1;				# Controls is actions are actually DOne
quiet=0;
opts=()				# Parsed options
pars=()				# Arguments excluding parsed options
let i=1				# Index into parameters
#
# Set variables specific to this script
#
DO_NORMAL=1;
DO_FONTS=0;
DO_ACCENTS=0;

baseURI=http://www.lyx.org/cgi-bin/viewcvs.cgi/lyx-devel
lyxDir=~lyx/www/sourcedoc
fileName_C=LyXAction.C
fileName_ui=stdtoolbars.ui
srcDir=src
uiDir=lib/ui
sedFile_C=`dirname $0`/${S0%.sh}_C.sed
sedFile_ui=`dirname $0`/${S0%.sh}_ui.sed
USE_WGET=0;
rev=HEAD;
outputWiki=0;
tmpResult=/tmp/lfuns.$$.tmp
tmpResult_ui=/tmp/lfun_ui.$$.tmp
rmTempFiles=1;

#
# Parse arguments
#
while (($i <= $#)); do
    addOpt=0; addPar=0; addSkip=0; # N:o args to add to $opts/$pars or skip
    case "${@:$i:1}" in
	-q) addOpt=1; quiet=1;;
	-v) addOpt=1; let ++Verbosity;;
	+v) addOpt=1; let --Verbosity;;
	-d) addOpt=1; set -x;;
	-n) addOpt=1; DO=0;;
	+n) addOpt=1; DO=1;;
	-r) addOpt=2; USE_WGET=1; rev=${@:(($i+1)):1};;
	--ui)		addOpt=1; USE_UI=1;;
	--normal)	addOpt=1; DO_NORMAL=1;;
	--fonts)	addOpt=1; 	     DO_FONTS=1;;
	--accents)	addOpt=1; 			   DO_ACCENTS=1;;
	--no-normal)	addOpt=1; DO_NORMAL=0;;
	--no-fonts)	addOpt=1; 	     DO_FONTS=0;;
	--no-accents)	addOpt=1; 			   DO_ACCENTS=0;;
	--only-normal)	addOpt=1; DO_NORMAL=1; DO_FONTS=0; DO_ACCENTS=0;;
	--only-fonts)	addOpt=1; DO_NORMAL=0; DO_FONTS=1; DO_ACCENTS=0;;
	--only-accents)	addOpt=1; DO_NORMAL=0; DO_FONTS=0; DO_ACCENTS=1;;
	--wiki-output)  addOpt=1; outputWiki=1;;
	--wiki-page)	addSkip=1; outputPage=1;;
	--keep-tmp)	addOpt=1; rmTempFiles=0;;
	--test) addSkip=1; doTest=1;;
	--set-verbosity) addOpt=2; Verbosity=${@:(($i+1)):1};;
	-*) Error "Unknown option '%s'" "${@:$i:1}";;
	*) addPar=1;;
    esac
    (($addPar)) && pars=("${pars[@]}" "${@:$i:$addPar}"); 
    (($addOpt)) && opts=("${opts[@]}" "${@:$i:$addOpt}"); 
    (($addPar && $addOpt)) && Error 'Both $addPar and $addOpt are non-zero'
    ((!$addPar && !$addOpt && !$addSkip)) && \
	Error 'Both $addPar and $addOpt are zero'
    let i+=$addPar+$addOpt+$addSkip;
done

Verbose 3 'Arguments parsed: $Verbosity=%d\n$opts=%s\n$pars=%s' "$Verbosity" \
    "`printf \"'%s' \" \"${opts[@]}\"`" "`printf \"'%s' \" \"${pars[@]}\"`" 

#
# Check arguments
#

#
# Do whatever the script does...
#
if(($outputPage)); then
    cat <<EOF
!!!List of LyX/LyxFunctions in release $rev
[[include:Site/DefineWikiStyles]]
*[[ThisPage:#various LFUNs sorted alphabetically]]
*[[ThisPage:#font LFUNs related to font commands]]
*[[ThisPage:#accent LFUNs related to accent commands]]

The lists of [=LFUNs=] below were iniitally derived using the script
DevelTools/MakeLFUNsPage based on the $rev release of
[[LyxSrc:$fileName_C?rev=$rev&content-type=text/vnd.viewcvs-markup $srcDir/$fileName_C]].
EOF
    (($USE_UI)) && cat <<EOF
And in addition, the script extracted help-texts from [[LyxCVS:lyx-devel/$uiDir/$fileName_ui?rev=$rev&content-type=text/vnd.viewcvs-markup $uiDir/$fileName_ui]]. 

EOF

    cat <<EOF
Some of the commands below take arguments and others don't.
Feel free to add comments about what an LFUN does.

[[#various]]Various [=LFUNs=], sorted alphabetically:
EOF

    $0 "${opts[@]}" --wiki-output --only-normal
    cat <<EOF

[[#font]][=LFUNs=] related to font commands:
EOF
    $0 "${opts[@]}" --wiki-output --only-fonts
    cat <<EOF

[[#accent]]Accent [=LFUNs=]:[[<<]]
Try for instance, %key%M-x accent acute a%% &mdash; which should give you the letter a with an ''acute'' above it. If you don't give an argument ('@@a@@' in the example), the command will be applied to the next letter you type.
EOF
    $0 "${opts[@]}" --wiki-output --only-accents
    exit
fi;

#
# If we will create wiki output, then redirect output to a temporary file
# that will be processed at the end of the script
#
if(($outputWiki)); then
    Verbose 2 "Redirecting output to %s" "$tmpResult"
    exec 6>&1
    exec > $tmpResult
fi;

#
# Retrieve files
#
if(($USE_WGET)); then
    file_C=/tmp/$fileName_C-$rev-$$.C
    file_ui=/tmp/$fileName_ui-$rev-$$.ui
    wget -q -O $file_C "$baseURI/$srcDir/$fileName_C?rev=$rev"
    wget -q -O $file_ui "$baseURI/$uiDir/$fileName_ui?rev=$rev"
else
    file_C=$lyxDir/$srcDir/$fileName_C
    file_ui=$fileName_ui
fi;

# Use this function to output a table row in PmWiki-format
function PrintTableRow() {
    cmd=`printf '%-30s' "$1"`;
    printf "||%-35s||%-28s||%s ||\n" "$cmd" "$2" "$3"
}

# This function is used to extract mappings of user-commands from a C-file
function ExtractCommandsFromC() {
    (($DO)) && sed -f $sedFile_C $file_C | sort -u
}

# Function: Extracts help text of user-commands from a ui-file
# Syntax: ExtractDataFromUI command
function ExtractDataFromUI() {
    [[ -r $tmpResult_ui ]] || {
	sed -f $sedFile_ui $file_ui | sort -u > $tmpResult_ui;
}
    while read line; do
	eval data=($line);
	if [[ "${data[1]}" == ${1}\ * ]]; then
	    PrintTableRow "${data[1]}" "${data[0]}"
	fi;
    done < $tmpResult_ui
}

# Function: Extracts help text of user-commands from a ui-file
# Syntax: PrintWithDataFromUI command helpText
function PrintWithDataFromUI() {
    [[ -r $tmpResult_ui ]] || {	# Maybe create result of parsing .ui-file
	sed -f $sedFile_ui $file_ui | sort -u > $tmpResult_ui;
    }
    commandNotInFile_ui=1; msg=();
    while read line; do
	eval data=($line);
	if [[ "${data[1]}" == "${1}" ]]; then
	    commandNotInFile_ui=0;
	    PrintTableRow "${data[1]}" "${data[0]}";
	elif [[ "${data[1]}" == ${1}\ * ]]; then
	    msg=("${msg[@]}" "`PrintTableRow \"${data[1]}\" \"${data[0]}\"`");
	fi;
    done < $tmpResult_ui
    (($commandNotInFile_ui)) && PrintTableRow "$1" "$2"
    ((${#msg[@]})) && printf "%s\n" "${msg[@]}"
}

if (($DO && $DO_NORMAL)); then
    ExtractCommandsFromC | grep -v '^\(font\|accent\)-'
fi;

if (($DO && $DO_FONTS)); then
    ExtractCommandsFromC | grep '^\(font-\)'
fi;

if (($DO && $DO_ACCENTS)); then
    ExtractCommandsFromC | grep '^\(accent-\)'
fi;

if(($outputWiki)); then
    exec 1>&6 6>&-		# Restore stdout and close file.desc.6

    PrintTableRow "'''Command string'''" "'''Help text'''" "'''Comment'''"
    while read command helpText; do
	if (($USE_UI)); then
	    PrintWithDataFromUI "$command" "$helpText"
	else
	    PrintTableRow "$command" "$helpText"
	fi;
    done < $tmpResult
fi;

Verbose "Done."
(($rmTempFiles && $outputWiki)) && rm -f $tmpResult
(($rmTempFiles && $USE_WGET)) && rm -f $file_C
(($rmTempFiles && $USE_UI)) && rm -f $file_ui $tmpResult_ui
