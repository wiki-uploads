/^	ev_item const items/,/^	};/	{
	s/^	ev_item.*//
	t header
	s/^	};//
	t delete

# Remove lines starting with # (#if 0 etc)
	s/^#//
	t delete

# Find (one-line) mappings of user commands
# Ex. of matching line:           "apropos-a", 
	s/^[	 ]*{ LFUN_[^,]*, *"\([^"]*\)", *\(N_("\([^"]*\)")\)\?\(.*\)[ 	]*},\?[ 	]*$/\1	\3/
	t cont1
	N
	s/^[	 ]*{ LFUN_[^,]*, *"\([^"]*\)", *\(N_("\([^"]*\)")\)\?\(.*\)[ 	]*},\?[ 	]*$/\1	\3/
	t cont1
	N
	s/^[	 ]*{ LFUN_[^,]*, *"\([^"]*\)", *\(N_("\([^"]*\)")\)\?\(.*\)[ 	]*},\?[ 	]*$/\1	\3/
	t cont1
	l
	b quit

: cont1

# Skip empty lines
	s/^[ 	]*$//
	t delete

# Format a user command
#	s/^\([-a-z]\+\)$/||\1	||	||/
	p
	}

: delete
	d

: quit
	q

# Insert the header for a table
: header
	d
