#!/bin/bash
#
# Christian Riddertrom 2003-11-124
#
S0=`basename $0`
if [[ "$1" == "" || "$1" == "-h" || "$1" == "--help" ]]; then
    cat <<EOF
Fcn:	Extract list of lfuns from a .C-file
Options:
	-q	Be more quiet
	-v	Be more verbose
	-d	Debug script
	-n	Don't actually do anything
	-r rev	Use wget to retrieve release 'rev' from CVS web-interface
	--normal	Extract 'normal' functions
	--no-normal	Skip extracting 'normal' functions
	--only-normal	Only extract 'normal' functions
	--fonts, --no-fonts and --only-fonts  are similar to the above.
	--accents, --no-accents and --only-accents are similar to the above.
	--wiki-output	Produce output in PmWiki-format
	--wiki-page	Produce a complete wiki-page (-r is required, and
			no other options work)
Examples:
# Extract 'normal' lfuns from release 1.3.3 (downloads file from the web)
	$S0 -r lyx-1_3_3
# Same thing, but using the latest (VCS) version
	$S0 -r HEAD

Note: The output is normally the string for the user-command, followed
by a TAB and possibly a help string (for release lyx-1_3_3). However, if
the option '--wiki-output' is given, then a resulting line looks like this:
||<user-command><TABs>||<help-text><TABs>||

To create a wiki page, use this sequence:
	$S0 -r lyx-1_3_3 --wiki-page > LyxFunctionList1-3-3.pmwiki

EOF
    exit 0
fi

Version=1.0
[[ "$1" == --version ]] && { printf "%s\n" "$Version"; exit 0; }

# Use: Verbose [vLevel] formatString [arguments]
# 
# Execute 'printf formatString [arguments]' if $Verbosity >= $vLevel,
# where the default value of $vLevel is 1, and the default is assumed if
# the length of the first argument is not one.
# Ex:	Verbose 3 "Only printed if verbosity(%d) >= 3" "$Verbosity"
# Ex:	Verbose "Only printed if verbosity >= 1"
#
Verbosity=0			# Default is Verbosity=0, i.e. no messages
function Verbose() {
    if ((${#1} == 1)); then vLevel=$1; shift; else vLevel=1; fi
    if (($Verbosity >= $vLevel)); then printf "$1\n" "${@:2}"; fi;
}

function Log() {
    printf "$1\n" "${@:2}"
}

#
# Set default values for variables
#
DO=1;
DO_NORMAL=1;
DO_FONTS=0;
DO_ACCENTS=0;
quiet=0;
args=( )
dir0=~lyx/www/pmwiki		# pmwiki/-directory
lyxDir=~lyx/www/sourcedoc
fileName=LyXAction.C
srcDir=src
sedFile=`dirname $0`/make-lfun-list.sed
sedFileWiki=`dirname $0`/make-lfun-wiki.sed
baseURI=http://www.lyx.org/cgi-bin/viewcvs.cgi/lyx-devel
USE_WGET=0;
rev=HEAD;
outputWiki=0;
tmpResult=/tmp/lfuns.$$.tmp

# Parse arguments
while [[ "$1" != "" ]]; do
    case "$1" in
	"-q") quiet=1;;
	"-v") let ++Verbosity;;
	"-d") set -v;;
	"+n") DO=1;;
	"-n") DO=0;;
	-r)		USE_WGET=1; rev=$2; shift;;
	--normal)	DO_NORMAL=1;;
	--fonts)		     DO_FONTS=1;;
	--accents)				 DO_ACCENTS=1;;
	--no-normal)	DO_NORMAL=0;;
	--no-fonts)		     DO_FONTS=0;;
	--no-accents)				 DO_ACCENTS=0;;
	--only-normal)	DO_NORMAL=1; DO_FONTS=0; DO_ACCENTS=0;;
	--only-fonts)	DO_NORMAL=0; DO_FONTS=1; DO_ACCENTS=0;;
	--only-accents)	DO_NORMAL=0; DO_FONTS=0; DO_ACCENTS=1;;
	--wiki-output)  outputWiki=1;;
	--wiki-page)	outputPage=1;;
    esac
    shift;
done

if(($outputPage)); then
    cat <<EOF
!!!List of LyxFunctions in release $rev

The lists of [=LFUNs=] below were derived from from releaes $rev of [[LyxSrc:LyXAction.C?rev=lyx-1_3_3&content-type=text/vnd.viewcvs-markup src/LyXAction.C]]. 
Some of the commands below take arguments and others don't.
Feel free to add comments about what an LFUN does.

[[#various]]Various [=LFUNs=], sorted alphabetically:
EOF

    $0 -r $rev --wiki-output
    cat <<EOF

[[#font]][=LFUNs=] related to font commands:
EOF
    $0 -r $rev --wiki-output --only-fonts
    cat <<EOF

[[#accent]]Accent [=LFUNs=]:[[<<]]
Try for instance, @@M-x accent acute a@@ &mdash; which should give you the letter a with an ''acute'' above it. If you don't give an argument ('@@a@@' in the example), the command will be applied to the next letter you type.
EOF
    $0 -r $rev --wiki-output --only-accents
    exit
fi;

Verbose "Processing options now finished."
if(($outputWiki)); then
    Verbose "Redirecting output to %s" "$tmpResult"
    exec 6>&1
    exec > $tmpResult
fi;

if(($USE_WGET)); then
    file=/tmp/$fileName-$rev-$$.C
    wget -q -O $file "$baseURI/$srcDir/$fileName?rev=$rev"
else
    file=$lyxDir/$srcDir/$fileName
fi;

function ExtractEnums() {
    (($DO)) && sed -f $sedFile $file | sort
}


if (($DO && $DO_NORMAL)); then
    ExtractEnums | grep -v '^\(font\|accent\)-'
fi;

if (($DO && $DO_FONTS)); then
    ExtractEnums | grep '^\(font-\)'
fi;

if (($DO && $DO_ACCENTS)); then
    ExtractEnums | grep '^\(accent-\)'
fi;

if(($outputWiki)); then
    exec 1>&6 6>&-		# Restore stdout and close file.desc.6

    printf "||%-30s||%-28s||%s||\n" \
	"'''Command string'''" "'''Help text'''" "'''Comment'''"
    while read command helpText; do
	printf "||%-30s||%-28s||\n" "$command" "$helpText"
    done < $tmpResult
    rm $tmpResult
fi;

Verbose "Done."
if(($USE_WGET)); then
    rm -f $file
fi;
