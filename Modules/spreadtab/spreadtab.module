#\DeclareLyXModule[spreadtab.sty,fp.sty,xstring.sty]{Spreadtab}
#DescriptionBegin
#Adds commands which convert LyX tables to spreadtab tables. These
#allow columns of numbers to be summed and other spreadsheet-like
#calculations. The tables are built in LyX using its table toolbar. 
#The calculated values appear only in the pdf (ps, dvi etc) document
#not in LyX. A one-celled table provides a calculator of sorts.
#DescriptionEnd
#
#v.1.1 2011-10-30
#Assumes version 0.4a of spreadtab.sty and
#will fail with 0.3 and earlier versions. 
#Author: Andrew Parsloe <aparsloe@clear.net.nz>
#The author & maintainer of spreadtab.sty is 
#Christian Tellechea <unbonpetit@gmail.com>

Format 35

AddToPreamble
	\usepackage{spreadtab}
%for LyX
	\STeol{\tabularnewline}
%default text cell marker is @ but ` is neater
	\renewcommand{\STtextcell}{`}
EndPreamble

Style sLTable
  Category			Tables
  LatexType     	Command
  LatexName     	sLTable
  OptionalArgs  	1
  NextNoIndent		0
  TopSep 			0.5
  BottomSep			0.5
  Align				Center
  Preamble
    \makeatletter
	\def\sLt@b[#1]\begin#2#3#4\end#5{\spreadtab[#1]{{#2}{#3}}#4\endspreadtab}
    \newcommand{\sLTable}[2][]{\begin{center}\sLt@b[#1]#2\end{center}}
	\makeatother
  EndPreamble
End

Style sLTablehcol
  CopyStyle			sLTable
  LatexName     	sLTablehcol
  RequiredArgs      1 
  Preamble
    \makeatletter
	\def\sLt@bhcol[#1]#2\begin#3#4#5\end#6{\spreadtab[#1]{{#3}{#2}}#5\endspreadtab}
    \newcommand{\sLTablehcol}[3][]{\begin{center}\sLt@bhcol[#1]{#2}#3\end{center}}
	\makeatother
  EndPreamble
End

InsetLayout Flex:sLCalculate
  LyXType			custom
  LatexType     	Command
  LatexName     	sLdocalc
  ContentAsLabel	1
  Decoration		Classic
  Display			0
  LabelString		Calc
  Multipar			0
  PassThru			1
  ResetsFont		0
  Preamble
    \def\sLdocalc#1{\mbox{\kern -.6em%
	\spreadtab[\STsavecell\sLlastcalc{a1}]{{tabular}{c}}#1\tabularnewline\endspreadtab%
	\kern -.6em}%
	}
  EndPreamble
End

InsetLayout Flex:sLNumberFormat
  LyXType			custom
  LatexType     	Command
  LatexName     	sLdoNF
  ContentAsLabel	1
  Decoration		Classic
  Font
	Color			phantomtext
  End
  Display			0
  LabelString		Fmt
  Multipar			0
  PassThru			1
  ResetsFont		0
  Preamble
    \def\sLdoNF#1{\sLdofmt#1}
	\def\sLdofmt#1,#2{\def\sLsigfig{#1}%
		\if0#2\def\sLfillzeros{*}\else\def\sLfillzeros{}\fi%
		\expandafter\STautoround\sLfillzeros{\sLsigfig}}
  EndPreamble
End
