Welcome to ConvTex Release 3.

This is a utility to convert LaTex files produced by Lyx to OpenOffice format,
written by Nick Thomas.

To install it, copy the file ConvTex.zip to your home directory and unzip it there.
It will create a sub-folder named ConvLaTex in your ~/Documents directory
containing all the required files.  In addition it will place a link in your home
folder to the file ConvTex.lyx explaining things in more detail.

Have fun!
