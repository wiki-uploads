Welcome to ConvTex Release 3. 

This is a utility to convert LaTex files produced by Lyx to OpenOffice format, 
written by Nick Thomas. 

To install the utility, copy the file ConvTexWin98.zip to \My Documents 
and unzip it there (right click on it and select Extract All). 
It will create a sub-folder named 
  ConvTexWin\ConvLaTex 
containing all the required files. 

See the document ConvTex.lyx in the above folder for more information. 

Have fun!  

This page took 0.019 seconds to produce.
