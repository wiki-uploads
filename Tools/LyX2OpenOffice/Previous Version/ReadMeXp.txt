Welcome to ConvTex Release 3.

This is a utility to convert LaTex files produced by Lyx to OpenOffice format,
written by Nick Thomas.

It is assumed that your home directory is
  \Documents and Settings\foo\
where your user name replaces foo.

To install the utility, copy the file ConvTexWin.zip to your
home directory and unzip it there (right click on it and select Extract All).
It will create a sub-folder named
  ConvTexWin\ConvLaTex
in your home directory containing all the required files.
The file ConvTex.lyx in ConvTexWin explains things in more detail.

See the document ConvTex.lyx for more information.

Have fun!
