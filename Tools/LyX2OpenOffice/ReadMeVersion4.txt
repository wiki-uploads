Welcome to ConvTex Release 4.

This is a utility to convert LaTex files produced by Lyx to OpenOffice format,
written by Nick Thomas.  It runs in Python, so you need that package installed.

In this release, GUI widgets have been added to enable easier operation, in
  particular the file to be converted is chosen with a file dialog.  This does
  mean that the Tk and Tix GUI packages must installed, which is usually true.
  The program may still be run in a terminal by providing the file name manually,
  in which case the GUI file dialog is not invoked.  You can view the file convtex.lyx
  listed here to see if it's what you want.  It is best to download it to read it properly.

Linux:-
  To install it, copy the file ConvTex.zip to your home directory and unzip it there.

  It will create a sub-folder named ConvLaTex in your ~/Documents directory
  containing all the required files.  Refer to ConvTex.lyx there which explains
  things in detail.

  In the folder ~/Documents/ConvLaTex run Config.py to set up your configuration,
  or click on the ConfigLyx2OO script.  A GUI-based setup program will be executed.

  In addition ConvTex.lyx and ConfigLyx2OO will be copied to your home folder for
  ease of initial reference.  You may click on ConfigLyx2OO there to run the set-up
  script if desired.  Both may be deleted after initial use.

Windows:
  To install it, copy the file ConvTex.zip to your home or other directory and unzip
  it there (right click on it and select 'Extract All...': accept the suggested path).

  It will create a sub-folder named ConvTexWin\Files in your home folder
  containing all the required files.

  In that sub-folder double-click on Config.py to set up your configuration with
  a GUI-based utility.

  There is no longer any distinction needed between Windows XP and 98.

Mac
  Jeremy Malcolm kindly produced a script for Mac users, as Version 3 works on Mac,
  which is reproduced in the MacScript.txt document here.
  No configuration documents are required.
  Version 4 should work on Mac as it is, without the need for a script, but I have not
  received a report back on that.

Both:
  Config.py is new to this release, saving the need to edit configuration files as in
  earlier releases.  It creates those files based on the default path selections
  you specify with the file dialog, so you may locate the package anywhere you
  like.

  The Bash and bat script files are no longer needed to run the program: simply
  run TexConv.py and the whole process is carried out automatically.  However a
  very simple Bash script called 'ConvTex' is created for Linux users in case
  double-clicking a .py file opens it instead of executing it.

Have fun!
