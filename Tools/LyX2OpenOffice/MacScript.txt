--- ConvTex.old Sun Oct 22 06:37:52 2006
+++ ConvTex     Sun Jul 15 09:01:17 2007
@@ -7,7 +7,8 @@
   printf "\t[any symbol to prevent /tmp files being deleted]\n"
   exit 0
 fi
-cd ~/Documents/ConvLaTex
+# Commented by terminus
+#cd ~/Documents/ConvLaTex
 ./TexConv.py $1 > /tmp/null
 if [ -f /tmp/TexError ]; then
   cat /tmp/TexError
@@ -22,7 +23,10 @@
 fi
 cd /tmp/officeXML
 zip -r officeXML *  > /tmp/null
-mv officeXML.zip ~/Documents/$fn.sxw
+# Path changed by terminus
+#mv officeXML.zip ~/Documents/$fn.sxw
+path=`dirname $1`
+mv officeXML.zip $path/$fn.sxw
 if [ -z $2 ]; then
   cd /tmp/
   rm -r officeXML
