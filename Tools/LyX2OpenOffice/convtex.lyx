#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass article
\language english
\inputencoding auto
\fontscheme default
\graphics default
\paperfontsize default
\papersize Default
\paperpackage a4
\use_geometry 0
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Title

Conversion to OpenOffice
\layout Author

N C Thomas
\layout Date

13th September 2007
\layout Standard


\begin_inset LatexCommand \tableofcontents{}

\end_inset 


\layout Section

Introduction
\layout Standard

It is sometimes useful to be able to convert 
\emph on 
Lyx
\emph default 
 files to word-processor formats, particularly 
\emph on 
Word
\emph default 
.
 It is more straight forward to convert to 
\emph on 
OpenOffice
\emph default 
 format.
 
\emph on 
OpenOffice
\emph default 
 generally makes a good job of converting to 
\emph on 
Word
\emph default 
.
 The script 
\family sans 
\emph on 
ConvTex
\family default 
\emph default 
 performs the conversion from 
\emph on 
LaTex
\emph default 
 to 
\emph on 
OpenOffice
\emph default 
 i.e.
 the 
\emph on 
Lyx
\emph default 
 document must be exported to 
\emph on 
LaTex
\emph default 
 first.
 This allows 
\family typewriter 
tex
\family default 
 documents that have not been produced from 
\emph on 
Lyx
\emph default 
 to be converted, but sometimes that may not work as unknown assumptions
 are often made.
\layout Standard

The programs are written in 
\emph on 
Python
\emph default 
 for a PC running Linux or Windows�
\begin_inset Foot
collapsed true

\layout Standard

Wherever 
\emph on 
Windows
\emph default 
 is referred to in this document, read it as Windows�
\end_inset 

, or a Mac.
\layout Section

Version
\layout Standard

This is for Release 4.
 See 
\begin_inset LatexCommand \ref{Changes}

\end_inset 

 for changes and bug fixes.
\layout Section

Installation
\layout Standard

This may seem a bit like ringing someone up to find out their telephone
 number! Although you will have installed the program if you are reading
 this, there are one or two extra points to be made.
\layout Subsection

Components needed
\layout Itemize

You need to have Python installed.
 Linux users will probably find it included in their distribution.
\layout Itemize

Windows users can download it from e.g.
 
\family typewriter 
http://www.python.org/download/
\layout Itemize

The GUI dialogs require the 
\family typewriter 
Tk
\family default 
 and 
\family typewriter 
Tix
\family default 
 to packages to be installed.
\layout Subsection

Configuration Completion
\layout Standard

You need to run the script 
\emph on 
Config.py
\emph default 
 to specify your default paths and construct the configuration files used
 by the programs.
 See 
\begin_inset LatexCommand \ref{sub:Config.py}

\end_inset 

 for details.
\layout Subsection


\family sans 
Configuration Files
\layout Standard

There are two configuration files constructed by 
\emph on 
Config.py
\emph default 
:
\layout Subsubsection

ConvTexConfig
\layout Enumerate


\begin_inset LatexCommand \label{homeDir}

\end_inset 

The first line contains your home directory path.
 This is set to the folder within which you installed 
\family typewriter 
ConvTex.zip
\family default 
 for Linux or 
\family typewriter 
ConvTexWin.zip
\family default 
 for Windows.
\layout Enumerate


\begin_inset LatexCommand \label{defPath}

\end_inset 

The second line contains the full default path of your tex documents.
\layout Enumerate

The third line contains the default path where picture files contained in
 your documents are stored.
 If the program cannot find a path for a picture it assumes this as the
 default.
\layout Enumerate

The fourth line contains a printer set-up string for 
\emph on 
OpenOffice
\emph default 
.
 It defaults to an 
\family typewriter 
Epson EPL-6200
\family default 
 as the document needs something.
 It is probably best to change the printer, if you need it, from within
 
\emph on 
OpenOffice.
\layout Subsubsection

ConvTexPaths
\layout Standard


\begin_inset LatexCommand \label{tempPaths}

\end_inset 

This contains the various platform-dependent paths and presets that are
 needed internally by the Python programs.
\layout Section

Converting 
\emph on 
Lyx
\emph default 
 Documents
\layout Standard


\begin_inset LatexCommand \label{conversion}

\end_inset 

This is done as follows:
\layout Subsection

Linux
\layout Itemize

Export your 
\emph on 
Lyx
\emph default 
 file to 
\emph on 
LaTex
\layout Itemize

Run the program 
\emph on 
TexConv.py
\emph default 
 without any parameters, or click the 
\family typewriter 
ConvTex
\family default 
 bash script (which may be relocated, or you may create a link to it on
 your Desktop; do not create a link directly to 
\emph on 
TexConv.py
\emph default 
 as the default directory will be wrong); a file dialog will appear to select
 the 
\family typewriter 
tex
\family default 
 file to convert
\layout Itemize

Alternatively run 
\emph on 
TexConv.py
\emph default 
 from a shell console, specifying 
\family typewriter 
myfile
\family default 
 without the 
\family typewriter 
.tex
\family default 
 extension but optionally including the full path:
\begin_deeper 
\layout LyX-Code

./TexConv.py myfile
\end_deeper 
\layout LyX-Code

a file selection GUI will not then appear
\layout Itemize

If the conversion is successful then a text window will appear saying so,
 and the result will be an
\noun on 
 
\family typewriter 
sxw
\family default 
\noun default 
 file located in your 
\family typewriter 
Documents
\family default 
 directory, with the same name as your 
\emph on 
Lyx
\emph default 
 file.
 Otherwise a brief error message will appear in a text window, unless a
 program execution error has occurred, when you may need to inspect the
 file 
\family typewriter 
TexError
\family default 
 in the 
\begin_inset LatexCommand \ref{tempPath}

\end_inset 

 temporay folder.
 See also 
\begin_inset LatexCommand \ref{errors}

\end_inset 

.
\layout Itemize

If you wish to obtain a 
\emph on 
Word
\emph default 
 or other format file, open the 
\family typewriter 
\noun on 
sxw
\family default 
\noun default 
 file in 
\emph on 
OpenOffice
\emph default 
 and go from there.
\layout Subsection

Windows
\layout Itemize

Export your 
\emph on 
Lyx
\emph default 
 file to 
\emph on 
LaTex
\layout Itemize

Double-click on 
\emph on 
TexConv.py
\emph default 
 in your 
\begin_inset LatexCommand \ref{homeDir}

\end_inset 

home directory, or on a shortcut to it of you have made one; a file dialog
 will appear to select the 
\family typewriter 
tex
\family default 
 file to convert
\layout Itemize

Alternatively run 
\emph on 
TexConv.py
\emph default 
 from the command prompt, specifying 
\family typewriter 
myfile
\family default 
 without the 
\family typewriter 
.tex
\family default 
 extension but optionally including the full path:
\layout LyX-Code

TexConv.py myfile
\newline 

\newline 
a file selection GUI will not then appear
\layout Itemize

If the conversion is successful then a text window will appear saying so,
 and the result will be an 
\family typewriter 
odt
\family default 
 file located in your
\begin_inset LatexCommand \ref{homeDir}

\end_inset 

home directory, with the same name as your 
\emph on 
Lyx
\emph default 
 file.
 Otherwise a brief error message will appear in a text window, unless a
 program execution error has occurred, when you may need to inspect the
 file 
\family typewriter 
TexError
\family default 
 in the 
\begin_inset LatexCommand \ref{tempPath}

\end_inset 

temporay folder.
 See also 
\begin_inset LatexCommand \ref{errors}

\end_inset 

.
\layout Itemize

If you wish to obtain a 
\emph on 
Word
\emph default 
 or other format file, open the 
\family typewriter 
odt
\family default 
 file in 
\emph on 
OpenOffice
\emph default 
 and go from there.
\layout Subsection

If there are errors
\begin_inset LatexCommand \label{errors}

\end_inset 


\layout Standard

Examine the file 
\family typewriter 
TexError
\family default 
 in your 
\begin_inset LatexCommand \ref{tempPath}

\end_inset 

 temporary folder for a brief error report.
\layout Section

Viewing the Converted Document
\layout Standard

When you view the converted document with 
\emph on 
OpenOffice
\emph default 
 it may reformat itself after a short time, and you may lose your viewing
 position, especially if the document is large.
 If you have placed the cursor then left-arrow/right-arrow should restore
 your viewing point.
\layout Standard

The reason for this has not been discovered, but may arise from 
\begin_inset Quotes eld
\end_inset 

overkill
\begin_inset Quotes erd
\end_inset 

 in specifying the paragraph formats more completely in XML than is perhaps
 necessary (to save a lot of extra mucking about when parsing).
\layout Standard

Before submitting for publication you can make a trivial amendment and re-save
 the document, which overcomes this (that is, if you have not had to make
 other amendments anyway :-) )
\layout Section

Exceptions
\layout Standard

This project is only partially complete, but has reached a useful stage.
 For example the 
\emph on 
Lyx
\emph default 
 UserGuide part 1 can be converted successfully, which covers a lot of ground.
\layout Subsection

Features not implemented
\layout Standard

So far the following 
\emph on 
Lyx
\emph default 
 features have not been implemented:
\layout Itemize

document layout: some features are omitted (see
\begin_inset LatexCommand \ref{sec:Styles}

\end_inset 

), in particular
\begin_deeper 
\layout Itemize

split columns
\layout Itemize

change of colour
\layout Itemize

customised list labels
\layout Itemize

language (English assumed)
\end_deeper 
\layout Itemize

paragraph layout: only label widths have been implemented
\layout Itemize

character layout: mostly implemented (see
\begin_inset LatexCommand \ref{sec:Styles}

\end_inset 

), but colour, language and toggling are not implemented
\layout Itemize

document paging has not been calculated, so 
\family typewriter 
vfill
\family default 
 is not implemented, page references are converted into chapter or section
 references, and forced page breaks are not implemented
\layout Itemize

AMS standards have not been implemented as presumably that august body prefers
 documents in 
\emph on 
Lyx
\emph default 
, 
\emph on 
LaTex
\emph default 
 or 
\emph on 
Tex
\emph default 
 form rather than in word-processor format
\layout Itemize

hyphenation points
\layout Itemize


\begin_inset LatexCommand \label{mathcal}

\end_inset 

neither 
\family typewriter 
blackboard
\family default 
, 
\family typewriter 
calligraphic
\family default 
, 
\family typewriter 
fraktur
\family default 
 nor 
\family typewriter 
teletype
\family default 
 fonts are recognised by 
\emph on 
StarMath
\emph default 
 (used by 
\emph on 
OpenOffice
\emph default 
 for equations)
\layout Itemize

Algorithm and Floatfit Figure
\layout Subsection

Variations from 
\emph on 
Lyx
\layout Standard

Variations from 
\emph on 
Lyx
\emph default 
 are:
\layout Itemize

equation array lines are only labelled if referred to by cross-references
\layout Itemize

the use of sideways text in tables can only be used in table header rows,
 and only for the whole row; this is because 
\emph on 
OpenOffice
\emph default 
 does not permit otherwise (as far as the author has been able to discover)
\layout Itemize

the 
\family typewriter 
slanted
\family default 
 font is rendered as
\family typewriter 
 italic
\family default 
 (a suitable alternative in 
\emph on 
OpenOffice
\emph default 
 has not been found)
\layout Itemize

floats are not implemented by 
\emph on 
OpenOffice
\emph default 
, so figure and table floats are put in a paragraph-aligned frame to keep
 the caption and object together
\layout Itemize

margin notes are converted to footnotes
\layout Itemize

comments are ignored
\layout Subsection

Other Issues
\layout Standard

Other implementation aspects:
\layout Itemize

tables are not allowed to be split across pages
\layout Itemize

in the 
\family typewriter 
lyxcode
\family default 
 environment protected blanks are converted into soft ones as otherwise
 the word-processor can split long lines in the middle of words.
 However a protected blank at the start of a line is retained for obvious
 reasons.
\layout Itemize

The following graphics formats can be correctly sized:
\newline 

\family typewriter 
bmp, eps, gif, jpg 
\family default 
and
\family typewriter 
 jpeg, pcx, png, ps, xbm
\family default 
 
\newline 
other types are sized as 12 x 9 cm
\newline 
this is because no size information is available in the 
\emph on 
LaTex
\emph default 
 file
\layout Itemize


\emph on 
OpenOffice
\emph default 
 does not display 
\family typewriter 
eps
\family default 
 or 
\family typewriter 
ps
\family default 
 images
\family typewriter 
.
 eps
\family default 
 images appear as a sized box including details of the image, and will print
 correctly on a Postscript� printer.
 Otherwise convert the image to another format before including it.
\layout Itemize

If an object is defined which is not a standard 
\emph on 
Lyx
\emph default 
 one the program does its best but may produce funny results as it cannot
 guess what is really wanted.
 This applies in particular to 
\family typewriter 
tex
\family default 
 documents not produced by 
\emph on 
Lyx
\emph default 
.
 The first program is adaptive and adds unrecognised objects to its dictionaries.
\layout Section


\begin_inset LatexCommand \label{Changes}

\end_inset 

Changes between Releases
\layout Subsection

Release 1
\layout Standard

None, but this was for Linux users only.
\layout Subsection

Release 2
\layout Standard

The Python programs were made platform-independent and extra configuration-suppo
rt files were introduced for Linux and Windows (XP by default) 
\layout Subsection

Release 3
\layout Standard

The following bugs were fixed
\layout Itemize

Table column formats now recognise >{centering}, >{raggedleft} and >{raggedright
}
\layout Itemize

Paragraph Spacing (see 
\begin_inset LatexCommand \ref{spacing}

\end_inset 

) now works properly (for sizes specified in 
\family typewriter 
mm
\family default 
, 
\family typewriter 
cm
\family default 
, 
\family typewriter 
in
\family default 
 and 
\family typewriter 
cc
\family default 
 only).
\layout Itemize

Table and figure floats are now correctly sized
\layout Itemize

Small graphic items are now properly sized
\layout Itemize

Support for Windows 98 has been included
\layout Subsection

Release 4
\layout Itemize

GUI support has been introduced.
 This relies on the 
\family typewriter 
Tk
\family default 
 and 
\family typewriter 
Tix
\family default 
 packages being available on your computer, which is generally true.
\layout Itemize

The policy to keep 
\emph on 
TexConv.py
\emph default 
 independent from 
\emph on 
ConvTex.py
\emph default 
 has been relaxed in so far as 
\emph on 
TexConv.py
\emph default 
 now calls 
\emph on 
ConvTex.py
\emph default 
, which means you only have to run 
\emph on 
TexConv.py
\emph default 
 to convert a document.
\layout Itemize

All the final zipping and clean-up previously done in Bash or .bat script
 files is now incorporated into 
\emph on 
ConvTex.py
\emph default 
, so those files are no longer needed.
\layout Itemize

The program 
\emph on 
Config.py
\emph default 
 now enables the configuration files to suit your platform to be generated
 automatically.
\layout Itemize

Thus no distinction between WindowsXP and Windows98 is any longer necessary.
\layout Section

Programs
\layout Standard

The conversion is performed by two 
\emph on 
Python
\emph default 
 programs, and you need to have the 
\emph on 
Python
\emph default 
 system installed.
 They are both platform-independent.
\layout Subsection

TexConv.py
\layout Standard

This parses the 
\emph on 
LaTex
\emph default 
 file and stores the result in a pickled dictionary located in your temporay
 directory.
 The idea is to parse into a (large!) Python dictionary so that all similar
 objects are grouped together.
 Then other dictionaries can be used to convert styles, formats and special
 characters to suit the target file type.
 In other words this program makes no assumption about the target file type,
 and its results can be used to convert to any other file type, given a
 suitable program and supporting dictionaries to implement that.
 It contains options to print out its results either by direct calls from
 the terminal or from a supervising program of some kind.
 It generates the following files in your temporay directory for 
\family typewriter 
myfile
\family default 
:
\begin_inset LatexCommand \label{files}

\end_inset 


\layout List
\labelwidthstring MMMMMM


\family typewriter 
myfile.ftd
\family default 
 contains the pickled dictionary which is the principal result used in the
 second stage of conversion
\layout List
\labelwidthstring 00.00.0000


\family typewriter 
myfile.ftm
\family default 
 contains headings for use by a supervisor program (not supplied
\begin_inset LatexCommand \ref{c_progs}

\end_inset 

) to display the results
\layout List
\labelwidthstring 00.00.0000


\family typewriter 
myfile.ftr
\family default 
 contains a report of the object types found and incorporated in 
\family typewriter 
myfile.ftd
\family default 
, again for use by a supervisor program to inspect the results.
\layout Standard

The latter two files may be inspected directly if desired, but see
\begin_inset LatexCommand \ref{normal}

\end_inset 

 to do that.
 The first one is in Python pickled form and is not suitable for direct
 inspection.
\layout Subsection

ConvOffice.py
\layout Standard

This program reads in 
\family typewriter 
myfile.ftd
\family default 
 and produces the final 
\emph on 
OpenOffice
\emph default 
 file from it.
 It requires the following dictionaries and 
\family typewriter 
\noun on 
xml
\family default 
\noun default 
 templates as supplied:
\layout List
\labelwidthstring MMMMMMMM

EndOffice converts compound object end tags
\layout List
\labelwidthstring MMMMMMMM

EscOffice converts escape characters
\layout List
\labelwidthstring MMMMMMMM

FontSizeOffice converts font sizes
\layout List
\labelwidthstring MMMMMMMM

FontStyleMathsOffice converts styles for equations
\layout List
\labelwidthstring MMMMMMMM

FontStyleOffice converts text styles
\layout List
\labelwidthstring MMMMMMMM

StartOffice converts compound object start tags
\layout List
\labelwidthstring MMMMMMMM

sizes 
\family typewriter 
charCount, mathsSizes, mathsSymb
\family default 
 and 
\family typewriter 
nonAscii
\family default 
 contain sizing information
\layout List
\labelwidthstring MMMMMMMM

OfficeXML is a folder containing the template 
\family typewriter 
xml
\family default 
 files from which the final document is constructed
\layout Subsection


\begin_inset LatexCommand \label{sub:Config.py}

\end_inset 

Config.py
\layout Standard

Available in Release 4, this script provides a GUI based method of creating
 the configuration files 
\family typewriter 
ConvTexPaths
\family default 
 and 
\family typewriter 
ConvTexConfig
\family default 
, which are thus not supplied.
 You specify the three paths for:
\layout Itemize


\begin_inset LatexCommand \label{tempPath}

\end_inset 

the location for temporary files 
\layout Itemize

the default path for your tex files, which is used by 
\emph on 
TexConv
\emph default 
 as the initial folder in its file dialog box
\layout Itemize

the default path for graphics to be included in the document
\layout Standard

For each path a path-selection dialog is presented to navigate to the desired
 folder: double-click on the folder and close the widget; a confirmation
 button dialogue will then be presented with three buttons:
\layout Enumerate


\color green
YES
\color default
 to accept the result
\layout Enumerate


\color red
NO
\color default
 to reject it, in which case the path-selection dialog is presented again
\layout Enumerate


\color red
ABORT CONFIGURATION
\color default
 which closes the program leaving any previously constructed configuration
 files undisturbed i.e.
 this script may be re-run at any time to change the defaults.
\layout Standard

The configuration files are constructed from this information.
 The platform is deduced from the type of file path delimiter character
 encountered; if there is any doubt a radio-button dialog will be presented
 to select the platform.
 After configuration, the only significance of the platorm type is the type
 of file path delimiter used by the programs.
\layout Subsection

Errors
\layout Standard

If an error occurs in either of the Python programs, the file 
\family typewriter 
TexError
\family default 
 will exist in your temporay directory and any files constructed as part
 of the process up to the error will remain in that folder.
 A brief note of the error may be reported on the terminal, depending on
 where the error occurred, which will also be found in a file named 
\family typewriter 
null
\family default 
 in the temporary folder.
 An even briefer one will be found there in the file 
\family typewriter 
TexError
\family default 
 (which is really created for the benefit of the script and/or supervisor
 programs to determine unambiguously that an error has occurred; it is deleted
 if all goes well).
 If the error was a file error the offending file name will be reported.
\layout Section

Contact
\layout Standard

The author can be contacted at nctsm@safe-mail.net
\layout Standard

If you would like to be informed of upgrades and bug fixes, please tell
 me.
\layout Section

Styles
\begin_inset LatexCommand \label{sec:Styles}

\end_inset 


\layout Subsection

Document Layout
\layout Standard

The following Layout/Document options have been implemented:
\layout List
\labelwidthstring 00.00.0000

Page\SpecialChar ~
size all page size options are available; default is treated as A4
\layout List
\labelwidthstring 00.00.0000

Orientation 
\family typewriter 
landscape
\family default 
\emph on 
 
\emph default 
may be selected instead of 
\family typewriter 
portrait
\layout List
\labelwidthstring 00.00.0000

Margins 
\layout Itemize


\family typewriter 
top
\family default 
, 
\family typewriter 
bottom
\family default 
, 
\family typewriter 
inner
\family default 
 and 
\family typewriter 
outer
\family default 
 margins may be set
\layout Itemize

the only units recognised are 
\family typewriter 
mm
\family default 
,
\family typewriter 
 cm
\family default 
 or
\family typewriter 
 in
\family default 
ches; any other setting will default to 
\family typewriter 
cm
\layout Itemize

if the inner margin differs from the outer and two-sided paper has been
 selected then pages will be mirrored correctly
\layout List
\labelwidthstring 00.00.0000

Font\SpecialChar ~
size all are recognised; the default is assumed to be 
\family typewriter 
10pt
\layout List
\labelwidthstring 00.00.0000

font the default font is 
\noun on 
Times New Roman
\newline 
times, palatino, helvetica, avant
\noun default 
 [garde gothic], 
\noun on 
newcent
\noun default 
 [New Century Schoolbook] and 
\noun on 
bookman
\noun default 
 are all recognised; 
\noun on 
ae
\noun default 
 and 
\noun on 
pslatex
\noun default 
 are not and default to 
\noun on 
Times New Roman
\noun default 

\newline 
we have gained a font in 
\emph on 
OpenOffice
\emph default 
 by assuming the default is 
\noun on 
Times New Roman
\layout List
\labelwidthstring 00.00.0000

Line\SpecialChar ~
spacing all are recognised, default is single-spacing; custom settings
 are assumed to be specified by integers
\layout List
\labelwidthstring 00.00.0000

Quote\SpecialChar ~
style implemented
\layout Standard

Changes to other settings will not be recognised and the defaults are assumed:-
\layout Itemize

Layout
\family sans 
\SpecialChar \menuseparator
Page style
\layout Itemize

Layout
\family sans 
\SpecialChar \menuseparator
Separation
\layout Itemize

Paper
\family sans 
\SpecialChar \menuseparator
Default
\layout Itemize

Margins
\family sans 
\SpecialChar \menuseparator
Head sep
\layout Itemize

Margins
\family sans 
\SpecialChar \menuseparator
Head height
\layout Itemize

Margins
\family sans 
\SpecialChar \menuseparator
Foot skip
\layout Itemize

Language
\family sans 
\SpecialChar \menuseparator
Language
\layout Itemize

Language
\family sans 
\SpecialChar \menuseparator
Encoding
\layout Itemize

Bullets
\layout Itemize

Numbering
\layout Itemize

Bibliography
\layout Subsection

Paragraph Layout
\layout Itemize

The Label Width may be set
\layout Itemize


\begin_inset LatexCommand \label{spacing}

\end_inset 

Spacing using the 
\family typewriter 
custom
\family default 
 option only, which may be specified in 
\family typewriter 
mm, cm, in
\family default 
 and 
\family typewriter 
cc
\family default 
 only.
 The minimum spacing corresponds to 2.1mm
\layout Itemize

Otherwise the defaults are assumed.
\layout Subsection

Character Layout
\layout Standard

The following options are implemented in Layout/Character:
\layout List
\labelwidthstring 00.00.0000

Family all
\layout List
\labelwidthstring 00.00.0000

Series all
\layout List
\labelwidthstring 00.00.0000

Shape all, but 
\family typewriter 
slanted
\family default 
 is rendered as 
\family typewriter 
italic
\layout List
\labelwidthstring 00.00.0000

Colour black assumed
\layout List
\labelwidthstring 00.00.0000

Language English assumed
\layout List
\labelwidthstring 00.00.0000

Toggling defaults assumed
\layout Section

More on Dictionaries
\layout Standard

The dictionaries have stylised names as it was initially envisaged that
 other programs apart from 
\family typewriter 
ConvOffice.py
\family default 
 could be written to convert to other formats.
 During early testing a plain text version called 
\family typewriter 
ConvVanilla.py
\family default 
 was used before 
\family typewriter 
ConvOffice.py
\family default 
 was written.
 It required different dictionaries from 
\family typewriter 
ConvOffice.py
\family default 
, and template dictionaries were used as the starting point for constructing
 sets of customised dictionaries.
 Because of this open approach 
\family typewriter 
ConvOffice.py
\family default 
 reads in some dictionaries based on its own name, as it too was derived
 from a template program.
 However it is unlikely that other conversion programs will be needed or
 desired, so none of this stuff has been supplied, although the first Python
 program remains in generalised form.
\layout Section

Equations
\layout Standard

Equation objects have to be corrected for 
\begin_inset Quotes eld
\end_inset 

faulty
\begin_inset Quotes erd
\end_inset 

 syntax, as otherwise 
\emph on 
OpenOffice
\emph default 
 shows errors.
 These may not be errors in fact as it may genuinely be desired to print
 
\begin_inset Formula $\sum$
\end_inset 

 for example, yet 
\emph on 
OpenOffice
\emph default 
 expects an argument after the symbol and prints an error indicator.
 To avoid this, {} is inserted after 
\begin_inset Formula $\sum$
\end_inset 

 which does not show and satisfies 
\emph on 
OpenOffice
\emph default 
.
 There are numerous such potential corrections to be made, and each equation
 has to be parsed for them.
 The most tricky is to correct for un-matched brackets of any kind, which
 can be useful for tensor equations such as 
\begin_inset Formula $x_{(1}=\left\{ \begin{array}{cc}
\alpha_{0} & \alpha_{0}^{s}\end{array}\right\} $
\end_inset 

.
 So far unmatched left brackets (of any kind) are corrected (the unmatched
 bracket is enclosed in ordinary double quotes which do not show), but the
 syntax analyser cannot (yet) correct for unmatched right brackets.
 If you need them, enclose them in ordinary double quotes.
\layout Standard

I have noticed a problem when 
\emph on 
Lyx
\emph default 
 exports to 
\emph on 
Latex
\emph default 
:- including a reserved text word like 'and' in an equation can cause the
 rest of the equation to be set to Roman style rather than just that word;
 this causes confusion as 
\family typewriter 

\backslash 
mathrm
\family default 
 usually means render in non-italic Roman style until its scope ends, which
 in turn for 
\emph on 
StarMath
\emph default 
 implies putting an ordinary double quote " at the start and end of the
 scope, ruining the equation if the scope is wrong.
 I have left this interpretation of 
\family typewriter 

\backslash 
mathrm
\family default 
 in place as I'm not sure what else to do, so beware! The same applies to
 
\family typewriter 

\backslash 
mathtt
\family default 
, 
\family typewriter 

\backslash 
mathbb
\family default 
,
\family typewriter 
 
\backslash 
mathcal
\family default 
 and
\family typewriter 
 
\backslash 
mathfrac
\family default 
 which in any case cannot be rendered
\begin_inset LatexCommand \ref{mathcal}

\end_inset 

.
\layout Section


\begin_inset LatexCommand \label{confFiles}

\end_inset 

Configuration files
\layout Itemize

The Python programs require one parameter, which is the name of the file
 to be converted, without the 
\family typewriter 
.tex
\family default 
 extension.
 If this is supplied without a path then the default path (c.f.
 
\begin_inset LatexCommand \ref{defPath}

\end_inset 

) is assumed, otherwise the full path supplied will be used instead.
 When the 
\family typewriter 
ConvTex
\family default 
 script is used this is supplied as its parameter.
\layout Itemize

The programs use the home directory (c.f.
 
\begin_inset LatexCommand \ref{homeDir}

\end_inset 

) to control where to find things.
\layout Itemize

The programs use the paths 
\begin_inset LatexCommand \ref{tempPaths}

\end_inset 

 in the 
\emph on 
ConvTexPaths
\emph default 
 file to control where temporary files are to be created during the conversion
 process.
 They must be specified as follows
\begin_deeper 
\layout Itemize

Line 1: the path 
\family typewriter 
p
\family default 
 where temporary files are constructed
\layout Itemize

Line 2: 
\family typewriter 
|p|TexError
\family default 
, replacing 
\family typewriter 
|
\family default 
 with 
\family typewriter 
/
\family default 
 for Linux users, or 
\family typewriter 

\backslash 

\family default 
 for Windows users.
\layout Itemize

Line 3: 
\family typewriter 
|p|officeXML
\layout Itemize

Line 4: 
\family typewriter 
|p|pythonTmp
\layout Itemize

Line 5: 
\family typewriter 
zz|ConvLaTex|Dictionaries|
\family default 
 where 
\family typewriter 
zz
\family default 
 is the document sub-folder in your home directory
\layout Itemize

Line 6: the path separator used by your platform: 
\family typewriter 
/
\family default 
 for Linux users, or 
\family typewriter 

\backslash 

\family default 
 for Windows users.
\layout Itemize

Line 7: the separator for 
\family typewriter 
xml
\family default 
 paths, must be 
\family typewriter 
/
\layout Itemize

Line 8: the separator for 
\family typewriter 
url
\family default 
 paths, must be 
\family typewriter 
/
\the_end
