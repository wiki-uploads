#!/usr/local/bin/tcsh
#
# Note: You may have to modify the path above according to where
#	'tcsh' is installed on your system.
#
# Created by Christian Ridderstr�m, 2002
#
# Feel free to use this as you please.
#
# Note: For some reason this script requires the GNU version of sed,
# 	i.e. the AIX version does not work.
#
set S=$0; set S0=$S:t
set SF = ()
#
# Default value for location of bind-files
#
if($?LYXDIR) then
    set bindDir = $LYXDIR/bind
else
    set bindDir = /usr/local/share/lyx/bind
endif
if("$1" == "" || "$1" == "-h" || "$1" == "--help") then
    cat <<EOF
Fcn:	Parse a .bind-file and output a list of bindings.
Options:
	-o odir	Output directory (default is '.')
	-d	Debug this script
	-v	Verbose mode
	-e ext	Use LyX to export to format 'ext'
		('ext' can be whatever LyX works with)

Ex:	>$S0 fil1.bind fil2.bind ... filN.bind
	Will output fil1.tex, fil2.tex ... filN.tex

Ex:	>$S0 -e pdf fil1.bind fil2.bind ... filN.bind
	Will output fil1.pdf, fil2.pdf ... filN.pdf

EOF
    exit
cat <<EOF
EOF
endif

set outDir="."; set Verbose=0; set parseFiles=1;
#
# Parse arguments
#
while("$1" != "")
    switch("$1")
	case "-d":
	    set verbose=1; set SF=($SF "-d"); shift; breaksw;
	case "-v":
	    @ Verbose++; set SF=($SF "-v"); shift; breaksw;
	case "-o":
	    set outDir=$2; set SF=($SF "-o $outDir"); shift; shift; breaksw;
	case "-e":
	    set ext="$2"; shift; shift; breaksw
	case "-L":
	    set LyxTemplate="$2"; shift; shift; breaksw
	case "-":
	    set parseFiles=0; set outFile="$2"; shift; shift; breaksw;
	default:
	    set bindFiles=($*); break; # Remaining arguments are .bind-files
    endsw
end

if($parseFiles) then
    set tmpFile=/tmp/$$
    set tmpSED=$tmpFile.sed
    if($Verbose) printf "Temporary files: %s and %s\n" "$tmpFile" "$tmpSED"

    set B="\\"; set BB="\\\\";
    cat >$tmpSED <<EOF
# Replace whitespaces with single space, and remove last whitespace
	s/[ 	]\+/ /g
	s/[ ]\+\$//
# Handle key bindings
/${B}bind /   { 
	s/${B}bind "\([^"]\+\)" \(".\+\$\)/${B}item[\1]	${B}verb\2/
	}
#
/${B}bind_file \+/   {					
	s/${B}bind_file \+\(.\+\)/${B}item[Include file]	${B}verb"\1"/
	}
# Remove multiple " inside \verb
	s/\(	${B}verb"[^"]\+\)"\(.*\$\)/\1\2"/g
# Escape ~ within the argument to \item
	s/^\([^~]\+\)~\(.\+${B}verb\)/\1$B~{}\2/
	s/^\([^_]\+\)_\(.\+${B}verb\)/\1${B}_\2/
# Print resulting item-lines
/^${B}item/	p
# and ignore the rest
	d
EOF
    if($Verbose > 1) cat $tmpSED

    foreach fil ( $bindFiles )
	set outFile="$outDir/${fil:t:r}.tex"
	printf "Parsing: %-25s -> %s\n" "$fil" "$outFile"
	cat > $outFile <<EOF
\chead{Bind file: ${fil:as/_/��/:as/��/\_/}}
\begin{list}{}{
    \setlength{\baselineskip}{0cm}
    \setlength{\itemsep}{0cm}
    \settowidth{\labelwidth}{0000.00000.0000}
    \setlength{\leftmargin}{\labelwidth}
    \addtolength{\leftmargin}{\labelsep}
    \renewcommand{\makelabel}[1]{#1\hfil}}
\item[Key] Description
EOF
	cat $fil					\
	    | grep "^[	 ]*\\bind" 			\
	    | grep -v "self-insert"			\
	    | sed -f $tmpSED >> $outFile

	printf "\\end{list}\n" >> $outFile
	if($?ext) then
	    mv $outFile bindings.inc
	    lyx -e $ext bindings.lyx
	    printf "Renaming bindings.lyx to %s\n" "$outFile:r.$ext"
	    mv bindings.$ext $outFile:r.$ext
	endif
    end
endif

