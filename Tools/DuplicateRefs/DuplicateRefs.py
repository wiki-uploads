#!/usr/bin/env python

# MAKE BACKUP OF YOUR LyX FILES BEFORE USING THIS SCRIPT! #

# This script receives a list of *.lyx filenames in command line.
# In each .lyx file it searches for labels consecutively following one after another.
# Then it removes all but the first label and in all supplied .lyx files corrects
# the references to refer to this label.
# It's useful to remove duplicate labels.

# Note that this script works with patched pyparsing.py (added addCondition() method).
# See last SVN for pyparsing or the bundled pyparsing.py file.

import sys
import pyparsing # parsley

all_files = sys.argv[1:]

if not all_files:
    print "Usage: DuplicateRefs.py FILE.lyx ...\n"
    sys.exit(1)

#CharsIn = lambda *args, **kwargs: pyparsing.Word(*args, **kwargs).leaveWhitespace()

#Line = pyparsing.Optional(pyparsing.CharsNotIn("\n"), "") + pyparsing.LineEnd().suppress() + pyparsing.NotAny(pyparsing.StringEnd())
Line = pyparsing.Optional(pyparsing.CharsNotIn("\n"), "") + pyparsing.LineEnd().suppress()

def ConstantLine(text):
    return Line.copy().addCondition(lambda self, loc, toks: toks and toks[0] == text)

def NamedLineParser(keyword):
    return pyparsing.Keyword(keyword).suppress() + pyparsing.White(" ").suppress() + \
        pyparsing.Literal('"').suppress() + pyparsing.CharsNotIn('"').setResultsName('label') + pyparsing.Literal('"').suppress()

LabelNameLineParser     = NamedLineParser('name')
ReferenceNameLineParser = NamedLineParser('reference')

LabelNameLine     = Line.copy().addParseAction(lambda self, loc, toks: LabelNameLineParser    .parseString(toks[0], True)['label'])
ReferenceNameLine = Line.copy().addParseAction(lambda self, loc, toks: ReferenceNameLineParser.parseString(toks[0], True)['label'])

Label = \
    ConstantLine("\\begin_inset CommandInset label").suppress() + \
    ConstantLine("LatexCommand label").suppress() + \
    LabelNameLine() + \
    ConstantLine("").suppress() + \
    ConstantLine("\\end_inset").suppress()

Reference = \
    ConstantLine("\\begin_inset CommandInset ref").suppress() + \
    ConstantLine("LatexCommand ref").suppress() + \
    ReferenceNameLine() + \
    ConstantLine("").suppress() + \
    ConstantLine("\\end_inset").suppress()

ConsequtiveLabels = Label + pyparsing.OneOrMore(pyparsing.White() + Label)

#parser = pyparsing.ZeroOrMore(ConsequtiveLabels.copy().addParseAction(mylambda) | Line) + pyparsing.StringEnd()

# First load all files into memory.
# Then scan all files and create a dictionary of label replacements (by the way removing duplicate labels).
# Then rename all labels.

lyx_texts = []

for file_name in all_files:
    lyx_texts.append(open(file_name).read())
    #parser.parseFile(file_name)

class InitialParserWrapper:
    def __init__(self):
        self.output = ""
        self.replacements = {}
        self.parser = pyparsing.ZeroOrMore(ConsequtiveLabels.copy().addParseAction(lambda arr: self.process_consequtive(arr)) | \
                                           Line.copy().addParseAction(lambda t: self.output_line(t))) + \
                      pyparsing.StringEnd()
        self.parser.parseWithTabs()
        #self.parser = pyparsing.ZeroOrMore(ConsequtiveLabels.copy().addParseAction(mylambda) | Line.copy().addParseAction(lambda t: self.output_line(t))) + \
                      #pyparsing.StringEnd()
    def output_line(self, tok):
        self.output += tok[0] + "\n"
    def process_consequtive(self, labels):
        for l in labels:
            self.replacements[l] = labels[0] # replace with the first label only
        self.output += "\\begin_inset CommandInset label\n" + "LatexCommand label\n" + 'name "'+labels[0]+'"\n' + "\n" + "\\end_inset\n"

wrapper = InitialParserWrapper()
for i in range(len(lyx_texts)):
    wrapper.output = ""
    wrapper.parser.parseString(lyx_texts[i])
    lyx_texts[i] = wrapper.output[:-1] # HACK: remove last "\n"
    #sys.stdout.write(lyx_texts[i]); sys.exit(0)

def my_replace(t):
    if t in wrapper.replacements:
        return wrapper.replacements[t]
    else:
        return t

for i in range(len(lyx_texts)):
    output = ""
    def output_line(tok):
        global output
        output += tok[0] + "\n"
    def output_ref(labels):
        global output
        output += "\\begin_inset CommandInset ref\n" + "LatexCommand ref\n" + 'reference "'+my_replace(labels[0])+'"\n' + "\n" + "\\end_inset\n"
    parser2 = pyparsing.ZeroOrMore(Reference.copy().addParseAction(output_ref) |
                                   Line.copy().addParseAction(output_line))
    parser2.parseWithTabs()
    parser2.parseString(lyx_texts[i])
    lyx_texts[i] = output[:-1] # HACK: remove last "\n"

for i in range(len(lyx_texts)):
    with open(all_files[i], 'w') as file:
        file.write(lyx_texts[i])

#class MyBaseParser(pyparsing.ParserElement):
